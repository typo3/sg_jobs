<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Controller\Ajax;

use Psr\Http\Message\ResponseInterface;
use SGalinski\SgAjax\Controller\Ajax\AbstractAjaxController;
use SGalinski\SgJobs\Service\FileAndFolderService;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Resource\Exception\ExistingTargetFolderException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderWritePermissionsException;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Uploads for the applicationForm
 */
class UploadController extends AbstractAjaxController {
	public const JOB_APPLICATION_TEMP_FOLDER = 'temp';

	/**
	 * @var mixed|string
	 */
	public $jobFolderPath = 'JobApplication';

	/**
	 * @var FileAndFolderService
	 */
	protected FileAndFolderService $fileAndFolderService;

	/**
	 * @param FileAndFolderService $fileAndFolderService
	 * @return void
	 */
	public function injectFileAndFolderService(FileAndFolderService $fileAndFolderService): void {
		$this->fileAndFolderService = $fileAndFolderService;
	}

	/**
	 * Initializes the class
	 */
	public function __construct() {
		$extensionConfiguration = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_jobs'] ?? [];
		if (isset($extensionConfiguration['jobFolderPath']) && $extensionConfiguration['jobFolderPath'] !== '') {
			$this->jobFolderPath = $extensionConfiguration['jobFolderPath'];
		} else {
			$this->jobFolderPath = 'JobApplication';
		}
	}

	/**
	 * Returns the calculated max file size.
	 *
	 * @return int
	 */
	protected function getMaxFileSize(): int {
		$maxFileSize = (int) $this->settings['fileUpload']['maxfileSize'];
		if ($maxFileSize <= 0 && isset($GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'])) {
			$maxFileSize = (int) $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize']; // Example value: 800000
			return ($maxFileSize * 1000);
		}

		return $maxFileSize * 1000 * 1000;
	}

	/**
	 * Uploads a new cover letter.
	 *
	 * @return ResponseInterface
	 * @throws ExistingTargetFolderException
	 * @throws InsufficientFolderAccessPermissionsException
	 * @throws InsufficientFolderWritePermissionsException
	 */
	public function uploadCoverletterAction(): ResponseInterface {
		$this->handleAnyDropZoneUpload();
		return $this->htmlResponse();
	}

	/**
	 * Uploads a new cd.
	 *
	 * @return ResponseInterface
	 * @throws ExistingTargetFolderException
	 * @throws InsufficientFolderAccessPermissionsException
	 * @throws InsufficientFolderWritePermissionsException
	 */
	public function uploadCvAction(): ResponseInterface {
		$this->handleAnyDropZoneUpload();
		return $this->htmlResponse();
	}

	/**
	 * Uploads a new certificate.
	 *
	 * @return ResponseInterface
	 * @throws ExistingTargetFolderException
	 * @throws InsufficientFolderAccessPermissionsException
	 * @throws InsufficientFolderWritePermissionsException
	 */
	public function uploadCertificateAction(): ResponseInterface {
		$this->handleAnyDropZoneUpload();
		return $this->htmlResponse();
	}

	/**
	 * since all files are handled the same way, we can use a single function!
	 *
	 * @return void
	 * @throws ExistingTargetFolderException
	 * @throws InsufficientFolderAccessPermissionsException
	 * @throws InsufficientFolderWritePermissionsException
	 */
	private function handleAnyDropZoneUpload(): void {
		$success = FALSE;
		$filePath = '';

		if (\count($_FILES) > 0) {
			$firstFile = current($_FILES);
			$pathInfo = pathinfo($firstFile['name']);

			$storage = $this->fileAndFolderService->getStorage();
			$fileName = $storage->sanitizeFileName(
				strtolower(
					str_replace(' ', '_', trim($pathInfo['filename'] . '.' . strtolower($pathInfo['extension'] ?? '')))
				)
			);
			// if the job application folder is not existing, create it
			if (!$storage->hasFolder($this->jobFolderPath)) {
				$folder = $storage->createFolder($this->jobFolderPath);
			} else {
				$folder = $storage->getFolder($this->jobFolderPath);
			}

			// if temp folder is not existing, create one
			if ($folder && !$storage->hasFolderInFolder(self::JOB_APPLICATION_TEMP_FOLDER, $folder)) {
				$storage->createFolder(self::JOB_APPLICATION_TEMP_FOLDER, $folder);
			}

			$filePath = Environment::getPublicPath() . '/fileadmin/' .
				$this->jobFolderPath . DIRECTORY_SEPARATOR .
				self::JOB_APPLICATION_TEMP_FOLDER . DIRECTORY_SEPARATOR . $fileName;
			$tempFilePath = $firstFile['tmp_name'];
			$success = GeneralUtility::upload_copy_move($tempFilePath, $filePath);
		}

		$this->returnData(
			[
				'success' => $success,
				'path' => $filePath,
			]
		);
	}
}
