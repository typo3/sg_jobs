<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Controller;

use Psr\Http\Message\ResponseInterface;
use SGalinski\SgJobs\Domain\Repository\JobRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * The JobTeaser plugin controller
 */
class JobTeaserController extends ActionController {
	/**
	 * @var JobRepository
	 */
	protected $jobRepository;

	/**
	 * @param JobRepository $jobRepository
	 */
	public function injectJobRepository(JobRepository $jobRepository) {
		$this->jobRepository = $jobRepository;
	}

	/**
	 * Get marked offers and display them
	 *
	 * @return ResponseInterface|null
	 */
	public function indexAction(): ?ResponseInterface {
		$filters = [];
		if (isset($this->settings['filterByDepartment']) && $this->settings['filterByDepartment'] !== '') {
			$filters['filterByDepartment'] = $this->settings['filterByDepartment'];
		}

		if (isset($this->settings['filterByExperienceLevel']) && $this->settings['filterByExperienceLevel'] !== '') {
			$filters['filterByExperienceLevel'] = $this->settings['filterByExperienceLevel'];
		}

		if (isset($this->settings['filterByLocation']) && $this->settings['filterByLocation'] !== '') {
			$filters['filterByLocation'] = $this->settings['filterByLocation'];
		}

		foreach ($filters as &$filter) {
			$filter = $filter ? GeneralUtility::trimExplode(',', $filter) : NULL;
			foreach ($filter as &$value) {
				$value = (int) $value;
			}

			unset($value);
		}

		unset($filter);

		$totalAmountOfOffers = $this->jobRepository->countAll($filters);

		$querySettings = $this->jobRepository->createQuery()->getQuerySettings();
		$querySettings->setIgnoreEnableFields(FALSE);
		$this->jobRepository->setDefaultQuerySettings($querySettings);

		$featuredOffers = $this->jobRepository->findByFeaturedOffer($filters, $this->settings['jobLimit'] ?? 3);
		$this->view->assign('totalAmountOfOffers', $totalAmountOfOffers);
		$this->view->assign('filters', $filters);
		$this->view->assign('featuredOffers', $featuredOffers);

		return $this->htmlResponse();
	}
}
