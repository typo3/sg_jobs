<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * The Company model
 */
class Company extends AbstractEntity {
	/**
	 * @var string $jobId
	 */
	protected $jobId = '';

	/**
	 * @var string $name
	 */
	protected $name = '';

	/**
	 * @var string $zip
	 */
	protected $zip = '';

	/**
	 * @var string $city
	 */
	protected $city = '';

	/**
	 * @var string $street
	 */
	protected $street = '';

	/**
	 * @var string $state
	 */
	protected $state = '';

	/**
	 * @var string $country
	 */
	protected $country = '';

	/**
	 * @var string $description
	 */
	protected $description = '';

	/**
	 * @var Contact $contact
	 */
	protected $contact;

	/**
	 * @var string $identifyingUrl
	 */
	protected $identifyingUrl;

	/**
	 * @return string
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity(string $city) {
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getState() {
		return $this->state;
	}

	/**
	 * @param string $state
	 */
	public function setState(string $state) {
		$this->state = $state;
	}

	/**
	 * @return string
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * @param string $country
	 */
	public function setCountry(string $country) {
		$this->country = $country;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description) {
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getStreet() {
		return $this->street;
	}

	/**
	 * @param string $street
	 */
	public function setStreet(string $street) {
		$this->street = $street;
	}

	/**
	 * @return Contact
	 */
	public function getContact() {
		return $this->contact;
	}

	/**
	 * @param Contact $contact
	 */
	public function setContact(Contact $contact) {
		$this->contact = $contact;
	}

	/**
	 * @return string
	 */
	public function getJobId() {
		return $this->jobId;
	}

	/**
	 * @param string $jobId
	 */
	public function setJobId(string $jobId) {
		$this->jobId = $jobId;
	}

	/**
	 * @return string
	 */
	public function getZip() {
		return $this->zip;
	}

	/**
	 * @param string $zip
	 */
	public function setZip($zip) {
		$this->zip = $zip;
	}

	/**
	 * @return string
	 */
	public function getIdentifyingUrl(): string {
		return $this->identifyingUrl;
	}

	/**
	 * @param string $identifyingUrl
	 */
	public function setIdentifyingUrl(string $identifyingUrl): void {
		$this->identifyingUrl = $identifyingUrl;
	}
}
