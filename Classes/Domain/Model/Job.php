<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Domain\Model;

use TYPO3\CMS\Extbase\Annotation\ORM\Lazy;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * The Job model
 */
class Job extends AbstractEntity {
	/**
	 * @var string $title
	 */
	protected $title = '';

	/**
	 * @var string $jobId
	 */
	protected $jobId = '';

	/**
	 * @var ObjectStorage<FileReference>
	 */
	#[Lazy]
	protected $attachment;

	/**
	 * @var string $task
	 */
	protected $task = '';

	/**
	 * @var string $qualification
	 */
	protected $qualification = '';

	/**
	 * @var Department
	 */
	protected $department;

	/**
	 * @var bool
	 */
	protected $featuredOffer = FALSE;

	/**
	 * @var bool
	 */
	protected $hideApplyByEmail = TRUE;

	/**
	 * @var bool
	 */
	protected $hideApplyByPostal = TRUE;

	/**
	 * @var string $alternativeStartDate
	 */
	protected $alternativeStartDate = '';

	/**
	 * @var int $startDate
	 */
	protected $startDate = 0;

	/**
	 * @deprecated, use mulitple Companies with different Locations instead
	 * @var string $location
	 */
	protected $location = '';

	/**
	 * @var int $sorting
	 */
	protected $sorting;

	/**
	 * @var ObjectStorage<Company>
	 */
	#[Lazy]
	protected $company;

	/**
	 * @var ObjectStorage<Job>
	 */
	#[Lazy]
	protected $relatedJobs;

	/**
	 * @var ObjectStorage<ExperienceLevel>
	 */
	#[Lazy]
	protected $experienceLevel;

	/**
	 * @var string $description
	 */
	protected $description = '';

	/**
	 * @var Contact $contact
	 */
	protected $contact;

	/**
	 * @var string
	 */
	protected $locationRequirements = '';

	/**
	 * @var bool
	 */
	protected $telecommutePossible = FALSE;

	/**
	 * @var bool
	 */
	protected $officeWorkPossible = TRUE;

	/**
	 * @var string $employmentTypes
	 */
	protected $employmentTypes = '';

	/**
	 * @var int $datePosted
	 */
	protected $datePosted = 0;

	/**
	 * @var int $datePosted
	 */
	protected $validThrough = 0;

	/**
	 * @var string $salaryCurrency
	 */
	protected $salaryCurrency = 'EUR';

	/**
	 * @var string $salaryUnit
	 */
	protected $salaryUnit = '';

	/**
	 * @var string $baseSalary
	 */
	protected $baseSalary = '';

	/**
	 * @var string $maxSalary
	 */
	protected $maxSalary = '';

	/** @var string */
	protected $applyExternalLink = '';

	/**
	 * Job constructor.
	 */
	public function __construct() {
		$this->initializeObject();
	}

	public function initializeObject() {
		$this->attachment = new ObjectStorage();
		$this->company = new ObjectStorage();
		$this->relatedJobs = new ObjectStorage();
		$this->experienceLevel = new ObjectStorage();
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title) {
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getJobId() {
		return $this->jobId;
	}

	/**
	 * @param string $jobId
	 */
	public function setJobId(string $jobId) {
		$this->jobId = $jobId;
	}

	/**
	 * @return ObjectStorage
	 */
	public function getAttachment(): ObjectStorage {
		if ($this->attachment instanceof LazyLoadingProxy) {
			$this->attachment->_loadRealInstance();
		}

		return $this->attachment;
	}

	/**
	 * @param ObjectStorage $attachment
	 */
	public function setAttachment(ObjectStorage $attachment): void {
		$this->attachment = $attachment;
	}

	/**
	 * @return string
	 */
	public function getTask() {
		return $this->task;
	}

	/**
	 * @param string $task
	 */
	public function setTask(string $task) {
		$this->task = $task;
	}

	/**
	 * @return string
	 */
	public function getQualification() {
		return $this->qualification;
	}

	/**
	 * @param string $qualification
	 */
	public function setQualification(string $qualification) {
		$this->qualification = $qualification;
	}

	/**
	 * @return int
	 */
	public function getStartDate() {
		return $this->startDate;
	}

	/**
	 * @param int $startDate
	 */
	public function setStartDate(int $startDate) {
		$this->startDate = $startDate;
	}

	/**
	 * @return ObjectStorage|NULL
	 */
	public function getCompany() {
		if ($this->company instanceof LazyLoadingProxy) {
			$this->company->_loadRealInstance();
		}

		return $this->company;
	}

	/**
	 * @return Company|NULL
	 */
	public function getFirstCompany(): ?Company {
		if ($this->company instanceof LazyLoadingProxy) {
			$this->company->_loadRealInstance();
		}

		if ($this->company->count() === 0) {
			return NULL;
		}
		return $this->company->offsetGet(0);
	}

	/**
	 * @param Company|ObjectStorage $company
	 */
	public function setCompany($company): void {
		if ($company instanceof Company) {
			$this->company->attach($company);
		} else {
			$this->company = $company;
		}
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description) {
		$this->description = $description;
	}

	/**
	 * @return Contact
	 */
	public function getContact() {
		return $this->contact;
	}

	/**
	 * @param Contact $contact
	 */
	public function setContact(Contact $contact) {
		$this->contact = $contact;
	}

	/**
	 * @return Department|NULL
	 */
	public function getDepartment() {
		return $this->department;
	}

	/**
	 * @param Department $department
	 */
	public function setDepartment(Department $department) {
		$this->department = $department;
	}

	/**
	 * @return string
	 */
	public function getAlternativeStartDate() {
		return $this->alternativeStartDate;
	}

	/**
	 * @param string $alternativeStartDate
	 */
	public function setAlternativeStartDate(string $alternativeStartDate) {
		$this->alternativeStartDate = $alternativeStartDate;
	}

	/**
	 * @return string
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * @param string $location
	 */
	public function setLocation(string $location) {
		$this->location = $location;
	}

	/**
	 * @return bool
	 */
	public function isFeaturedOffer() {
		return $this->featuredOffer;
	}

	/**
	 * @param bool $featuredOffer
	 */
	public function setFeaturedOffer($featuredOffer) {
		$this->featuredOffer = $featuredOffer;
	}

	/**
	 * @return bool
	 */
	public function isHideApplyByEmail() {
		return $this->hideApplyByEmail;
	}

	/**
	 * @param bool $hideApplyByEmail
	 */
	public function setHideApplyByEmail($hideApplyByEmail) {
		$this->hideApplyByEmail = (bool) $hideApplyByEmail;
	}

	/**
	 * @return bool
	 */
	public function isHideApplyByPostal() {
		return $this->hideApplyByPostal;
	}

	/**
	 * @param bool $hideApplyByPostal
	 */
	public function setHideApplyByPostal($hideApplyByPostal) {
		$this->hideApplyByPostal = (bool) $hideApplyByPostal;
	}

	/**
	 * @return int
	 */
	public function getSorting(): int {
		return $this->sorting;
	}

	/**
	 * @param int $sorting
	 */
	public function setSorting(int $sorting): void {
		$this->sorting = $sorting;
	}

	/**
	 * @return bool
	 */
	public function getTelecommutePossible(): bool {
		return $this->telecommutePossible;
	}

	/**
	 * @param bool $telecommutePossible
	 */
	public function setTelecommutePossible(bool $telecommutePossible): void {
		$this->telecommutePossible = $telecommutePossible;
	}

	/**
	 * @return bool
	 */
	public function getOfficeWorkPossible(): bool {
		return $this->officeWorkPossible;
	}

	/**
	 * @param bool $officeWorkPossible
	 */
	public function setOfficeWorkPossible(bool $officeWorkPossible): void {
		$this->officeWorkPossible = $officeWorkPossible;
	}

	/**
	 * @return string
	 */
	public function getEmploymentTypes(): string {
		return $this->employmentTypes;
	}

	/**
	 * @param string $employmentTypes
	 */
	public function setEmploymentTypes(string $employmentTypes): void {
		$this->employmentTypes = $employmentTypes;
	}

	/**
	 * @return int
	 */
	public function getDatePosted(): int {
		return $this->datePosted;
	}

	/**
	 * @param int $datePosted
	 */
	public function setDatePosted(int $datePosted): void {
		$this->datePosted = $datePosted;
	}

	/**
	 * @return int
	 */
	public function getValidThrough(): int {
		return $this->validThrough;
	}

	/**
	 * @param int $validThrough
	 */
	public function setValidThrough(int $validThrough): void {
		$this->validThrough = $validThrough;
	}

	/**
	 * @return string
	 */
	public function getSalaryCurrency(): string {
		return $this->salaryCurrency;
	}

	/**
	 * @param string $salaryCurrency
	 */
	public function setSalaryCurrency(string $salaryCurrency): void {
		$this->salaryCurrency = $salaryCurrency;
	}

	/**
	 * @return string
	 */
	public function getSalaryUnit(): string {
		return $this->salaryUnit;
	}

	/**
	 * @param string $salaryUnit
	 */
	public function setSalaryUnit(string $salaryUnit): void {
		$this->salaryUnit = $salaryUnit;
	}

	/**
	 * @return string
	 */
	public function getBaseSalary(): string {
		return $this->baseSalary;
	}

	/**
	 * @param string $baseSalary
	 */
	public function setBaseSalary(string $baseSalary): void {
		$this->baseSalary = $baseSalary;
	}

	/**
	 * @return string
	 */
	public function getMaxSalary(): string {
		return $this->maxSalary;
	}

	/**
	 * @param string $maxSalary
	 */
	public function setMaxSalary(string $maxSalary): void {
		$this->maxSalary = $maxSalary;
	}

	/**
	 * @return string
	 */
	public function getApplyExternalLink(): string {
		return $this->applyExternalLink;
	}

	/**
	 * @param string $applyExternalLink
	 */
	public function setApplyExternalLink(string $applyExternalLink): void {
		$this->applyExternalLink = $applyExternalLink;
	}

	/**
	 * @return ObjectStorage
	 */
	public function getExperienceLevel(): ObjectStorage {
		if ($this->experienceLevel instanceof LazyLoadingProxy) {
			$this->experienceLevel->_loadRealInstance();
		}

		return $this->experienceLevel;
	}

	/**
	 * @param ObjectStorage $experienceLevel
	 */
	public function setExperienceLevel(ObjectStorage $experienceLevel): void {
		$this->experienceLevel = $experienceLevel;
	}

	/**
	 * @param ExperienceLevel $experienceLevel
	 * @return void
	 */
	public function addExperienceLevel(ExperienceLevel $experienceLevel): void {
		if ($this->experienceLevel instanceof LazyLoadingProxy) {
			$this->experienceLevel->_loadRealInstance();
		}

		$this->experienceLevel->attach($experienceLevel);
	}

	/**
	 * @param ExperienceLevel $experienceLevel
	 * @return void
	 */
	public function removeExperienceLevel(ExperienceLevel $experienceLevel): void {
		if ($this->experienceLevel instanceof LazyLoadingProxy) {
			$this->experienceLevel->_loadRealInstance();
		}

		$this->experienceLevel->detach($experienceLevel);
	}

	/**
	 * @param ObjectStorage $relatedJobs
	 * @return void
	 */
	public function setRelatedJobs(ObjectStorage $relatedJobs): void {
		$this->relatedJobs = $relatedJobs;
	}

	/**
	 * @return ObjectStorage
	 */
	public function getRelatedJobs(): ObjectStorage {
		if ($this->relatedJobs instanceof LazyLoadingProxy) {
			$this->relatedJobs->_loadRealInstance();
		}

		return $this->relatedJobs;
	}

	/**
	 * @param Job $relatedJobs
	 * @return void
	 */
	public function addRelatedJobs(Job $relatedJobs): void {
		if ($this->relatedJobs instanceof LazyLoadingProxy) {
			$this->relatedJobs->_loadRealInstance();
		}

		$this->relatedJobs->attach($relatedJobs);
	}

	/**
	 * @param Job $relatedJobs
	 * @return void
	 */
	public function removeRelatedJobs(Job $relatedJobs): void {
		if ($this->relatedJobs instanceof LazyLoadingProxy) {
			$this->relatedJobs->_loadRealInstance();
		}

		$this->relatedJobs->detach($relatedJobs);
	}

	/**
	 * @return string
	 */
	public function getLocationRequirements(): string {
		return $this->locationRequirements;
	}

	/**
	 * @param string $locationRequirements
	 */
	public function setLocationRequirements(string $locationRequirements): void {
		$this->locationRequirements = $locationRequirements;
	}
}
