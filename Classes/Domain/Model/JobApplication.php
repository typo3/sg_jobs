<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Domain\Model;

use TYPO3\CMS\Extbase\Annotation\ORM\Lazy;
use TYPO3\CMS\Extbase\Annotation\Validate;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * The JobApplication model
 */
class JobApplication extends AbstractEntity {
	/**
	 * An internal id for external purposes
	 *
	 * @var string $jobId
	 */
	protected $jobId = '';

	/**
	 * @var string $jobTitle
	 */
	protected $jobTitle = '';

	/**
	 * @var ObjectStorage<Company>
	 */
	#[Lazy]
	protected $company;

	/**
	 * @var Job $job
	 */
	protected $job;

	/**
	 * @var string $gender
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $gender = '';

	/**
	 * @var string $firstName
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $firstName = '';

	/**
	 * @var string $lastName
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $lastName = '';

	/**
	 * @var string $street
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $street = '';

	/**
	 * @var string $city
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $city = '';

	/**
	 * @var string $zip
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $zip = '';

	/**
	 * @var string $country
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $country = '';

	/**
	 * @var string $nationality
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $nationality = '';

	/**
	 * @var string $education
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $education = '';

	/**
	 * @var string $birthDate
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $birthDate = '';

	/**
	 * @var string $phone
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	protected $phone = '';

	/**
	 * @var string $mobile
	 */
	protected $mobile = '';

	/**
	 * @var string $email
	 */
	#[Validate(['validator' => 'NotEmpty'])]
	#[Validate(['validator' => 'EmailAddress'])]
	protected $email = '';

	/**
	 * @var FileReference $coverLetter
	 */
	protected $coverLetter;

	/**
	 * @var FileReference $cv
	 */
	protected $cv;

	/**
	 * @var FileReference $certificate
	 */
	protected $certificate;

	/**
	 * @var string
	 */
	protected $message = '';

	/**
	 * @var bool
	 */
	#[Validate(['validator' => 'Boolean', 'options' => ['is' => TRUE]])]
	protected $privacyPolicy = FALSE;

	/**
	 * @var string
	 */
	protected $freetextField1 = '';

	/**
	 * @var string
	 */
	protected $freetextField2 = '';

	/**
	 * @var string
	 */
	protected $freetextField3 = '';

	/**
	 * @var string
	 */
	protected $freetextField4 = '';

	/**
	 * @var string
	 */
	protected $freetextField5 = '';

	/**
	 * Job application constructor.
	 */
	public function __construct() {
		$this->company = new ObjectStorage();
	}

	/**
	 * @return string
	 */
	public function getJobId() {
		return $this->jobId;
	}

	/**
	 * @param string $jobId
	 */
	public function setJobId($jobId) {
		$this->jobId = $jobId;
	}

	/**
	 * @return string
	 */
	public function getGender() {
		return $this->gender;
	}

	/**
	 * @param string $gender
	 */
	public function setGender(string $gender) {
		$this->gender = $gender;
	}

	/**
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * @param string $firstName
	 */
	public function setFirstName(string $firstName) {
		$this->firstName = $firstName;
	}

	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName(string $lastName) {
		$this->lastName = $lastName;
	}

	/**
	 * @return string
	 */
	public function getStreet() {
		return $this->street;
	}

	/**
	 * @param string $street
	 */
	public function setStreet(string $street) {
		$this->street = $street;
	}

	/**
	 * @return string
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity(string $city) {
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getZip() {
		return $this->zip;
	}

	/**
	 * @param string $zip
	 */
	public function setZip(string $zip) {
		$this->zip = $zip;
	}

	/**
	 * @return string
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * @param string $country
	 */
	public function setCountry(string $country) {
		$this->country = $country;
	}

	/**
	 * @return string
	 */
	public function getNationality() {
		return $this->nationality;
	}

	/**
	 * @param string $nationality
	 */
	public function setNationality(string $nationality) {
		$this->nationality = $nationality;
	}

	/**
	 * @return string
	 */
	public function getEducation() {
		return $this->education;
	}

	/**
	 * @param string $education
	 */
	public function setEducation(string $education) {
		$this->education = $education;
	}

	/**
	 * @return string
	 */
	public function getBirthDate() {
		return $this->birthDate;
	}

	/**
	 * @param string $birthDate
	 */
	public function setBirthDate($birthDate) {
		$this->birthDate = $birthDate;
	}

	/**
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone(string $phone) {
		$this->phone = $phone;
	}

	/**
	 * @return string
	 */
	public function getMobile() {
		return $this->mobile;
	}

	/**
	 * @param string $mobile
	 */
	public function setMobile(string $mobile) {
		$this->mobile = $mobile;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail(string $email) {
		$this->email = $email;
	}

	/**
	 * @return FileReference
	 */
	public function getCoverLetter() {
		return $this->coverLetter;
	}

	/**
	 * @param FileReference $coverLetter
	 */
	public function setCoverLetter(FileReference $coverLetter = NULL) {
		$this->coverLetter = $coverLetter;
	}

	/**
	 * @return FileReference
	 */
	public function getCv() {
		return $this->cv;
	}

	/**
	 * @param FileReference $cv
	 */
	public function setCV(FileReference $cv = NULL) {
		$this->cv = $cv;
	}

	/**
	 * @return FileReference
	 */
	public function getCertificate() {
		return $this->certificate;
	}

	/**
	 * @param FileReference $certificate
	 */
	public function setCertificate(FileReference $certificate = NULL) {
		$this->certificate = $certificate;
	}

	/**
	 * @return string
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * @param string $message
	 */
	public function setMessage(string $message) {
		$this->message = $message;
	}

	/**
	 * @return string
	 */
	public function getJobTitle(): string {
		return $this->jobTitle;
	}

	/**
	 * @param string $jobTitle
	 */
	public function setJobTitle(string $jobTitle) {
		$this->jobTitle = $jobTitle;
	}

	/**
	 * @return Job
	 */
	public function getJob() {
		return $this->job;
	}

	/**
	 * @param Job $job
	 */
	public function setJob(Job $job) {
		$this->job = $job;
	}

	/**
	 * @return ObjectStorage|NULL
	 */
	public function getCompany() {
		if ($this->company instanceof LazyLoadingProxy) {
			$this->company->_loadRealInstance();
		}

		return $this->company;
	}

	public function getFirstCompany(): ?Company {
		if ($this->company instanceof LazyLoadingProxy) {
			$this->company->_loadRealInstance();
		}

		if ($this->company->count() === 0) {
			return NULL;
		}
		return $this->company->offsetGet(0);
	}

	/**
	 * @return ObjectStorage
	 */
	public function getCompanies(): ObjectStorage {
		if ($this->company instanceof LazyLoadingProxy) {
			$this->company->_loadRealInstance();
		}

		return $this->company;
	}

	/**
	 * @param Company|ObjectStorage $company
	 */
	public function setCompany($company): void {
		if ($company instanceof Company) {
			$this->company->attach($company);
		} else {
			$this->company = $company;
		}
	}

	/**
	 * @return bool
	 */
	public function getPrivacyPolicy() {
		return $this->privacyPolicy;
	}

	/**
	 * @param bool $privacyPolicy
	 */
	public function setPrivacyPolicy($privacyPolicy) {
		$this->privacyPolicy = $privacyPolicy;
	}

	/**
	 * @return string
	 */
	public function getFreetextField1(): string {
		return $this->freetextField1;
	}

	/**
	 * @param string $freetextField1
	 * @return void
	 */
	public function setFreetextField1(string $freetextField1): void {
		$this->freetextField1 = $freetextField1;
	}

	/**
	 * @return string
	 */
	public function getFreetextField2(): string {
		return $this->freetextField2;
	}

	/**
	 * @param string $freetextField2
	 * @return void
	 */
	public function setFreetextField2(string $freetextField2): void {
		$this->freetextField2 = $freetextField2;
	}

	/**
	 * @return string
	 */
	public function getFreetextField3(): string {
		return $this->freetextField3;
	}

	/**
	 * @param string $freetextField3
	 * @return void
	 */
	public function setFreetextField3(string $freetextField3): void {
		$this->freetextField3 = $freetextField3;
	}

	/**
	 * @return string
	 */
	public function getFreetextField4(): string {
		return $this->freetextField4;
	}

	/**
	 * @param string $freetextField4
	 * @return void
	 */
	public function setFreetextField4(string $freetextField4): void {
		$this->freetextField4 = $freetextField4;
	}

	/**
	 * @return string
	 */
	public function getFreetextField5(): string {
		return $this->freetextField5;
	}

	/**
	 * @param string $freetextField5
	 * @return void
	 */
	public function setFreetextField5(string $freetextField5): void {
		$this->freetextField5 = $freetextField5;
	}
}
