<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Domain\Repository;

use SGalinski\SgJobs\Domain\Model\Company;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Company Repository
 */
class CompanyRepository extends Repository {
	/** @var array */
	protected $cache = [];

	/**
	 * Returns all countries filtered by page id
	 *
	 * @param array $pageUids
	 * @param array $filters
	 * @return array
	 * @throws AspectNotFoundException
	 */
	public function getAllCountries(array $pageUids, array $filters = []): array {
		$result = $this->getAllCompanies($pageUids, $filters)->toArray();

		$countryArray = [];
		/** @var Company $company */
		foreach ($result as $company) {
			$countryName = $company->getCountry();
			if ($countryName !== '') {
				// The filtering needs this specific key for some reason.
				$countryArray[$countryName] = $countryName;
			}
		}

		ksort($countryArray);
		return $countryArray;
	}

	/**
	 * Returns all filtered cities
	 *
	 * @param array $pageUids
	 * @param array $filters
	 * @return array
	 * @throws AspectNotFoundException
	 */
	public function getAllCities(array $pageUids, array $filters = []): array {
		$result = $this->getAllCompanies($pageUids, $filters)->toArray();

		$cityArray = [];
		/** @var Company $company */
		foreach ($result as $company) {
			$cityName = $company->getCity();
			if ($cityName !== '') {
				// The filtering needs this specific key for some reason.
				$cityArray[$cityName] = $company->getUid();
			}
		}

		ksort($cityArray);
		return array_flip($cityArray);
	}

	/**
	 * Returns all company names filtered by page id
	 *
	 * @param array $pageUids
	 * @param array $filters
	 * @return array
	 * @throws AspectNotFoundException
	 */
	public function getAllCompanyNames(array $pageUids, array $filters = []): array {
		$result = $this->getAllCompanies($pageUids, $filters)->toArray();

		$companyArray = [];
		/** @var Company $company */
		foreach ($result as $company) {
			$companyName = $company->getName();
			if ($companyName !== '') {
				// The filtering needs this specific key for some reason.
				$companyArray[$companyName] = $company->getUid();
			}
		}

		ksort($companyArray);
		return array_flip($companyArray);
	}

	/**
	 * Returns all companies filtered by page id
	 *
	 * @param array $pageUids
	 * @param array $filters
	 * @return object[]|QueryResultInterface
	 * @throws AspectNotFoundException
	 */
	public function getAllCompanies(array $pageUids, array $filters = []) {
		$hash = '';
		foreach ($pageUids as $pageUid) {
			$hash .= $pageUid . '_' . implode('-', $filters);
		}

		if (!isset($this->cache[$hash])) {
			$companyConstraints = [];
			$query = $this->createQuery();
			$querySettings = $query->getQuerySettings();
			$querySettings->setStoragePageIds($pageUids);
			$querySettings->setLanguageAspect(
				GeneralUtility::makeInstance(Context::class)->getAspect('language')
			);
			$query->setQuerySettings($querySettings);

			$query->setOrderings(
				[
					'sorting' => QueryInterface::ORDER_ASCENDING,
				]
			);

			$companyConstraints[] = $query->equals('pid', $pageUids);
			if (count($filters) > 0) {
				$constraints = [];
				foreach ($filters as $filter) {
					$constraints[] = $query->equals('uid', $filter);
				}

				$companyConstraints[] = $query->logicalOr(...$constraints);
			}

			$query->matching($query->logicalAnd(...$companyConstraints));
			$this->cache[$hash] = $query->execute();
		}

		return $this->cache[$hash];
	}
}
