<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class DepartmentRepository
 *
 * @package SGalinski\SgJobs\Domain\Repository
 * @author Kevin Ditscheid <kevin.ditscheid@sgalinski.de>
 */
class DepartmentRepository extends Repository {
	protected $defaultOrderings = [
		'sorting' => QueryInterface::ORDER_ASCENDING,
		'title' => QueryInterface::ORDER_ASCENDING
	];

	/**
	 * Finds all departments with optional filters
	 *
	 * @param array $filters
	 * @return object[]|QueryResultInterface
	 */
	public function findAllByFilter(array $filters = []) {
		if (count($filters) < 1) {
			return $this->findAll();
		}

		$query = $this->createQuery();
		$constraints = [];
		foreach ($filters as $filter) {
			$constraints[] = $query->equals('uid', $filter);
		}

		$query->matching($query->logicalOr(...$constraints));
		return $query->execute();
	}
}
