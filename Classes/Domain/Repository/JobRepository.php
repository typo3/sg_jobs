<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Domain\Repository;

use Doctrine\DBAL\Exception;
use SGalinski\SgJobs\Domain\Model\Job;
use TYPO3\CMS\Core\Context\LanguageAspect;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface as ExtbaseQueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Job Repository
 */
class JobRepository extends Repository {
	public const ORDER_BY_TITLE = 0;
	public const ORDER_BY_CRDATE = 2;
	public const ORDER_BY_SORTING = 1;

	/**
	 * Queries the job records based on filters (for the backend)
	 *
	 * @param int $recordPageId
	 * @param array $filters
	 * @param int $limit
	 * @param int $offset
	 * @return ExtbaseQueryResultInterface|object[]
	 * @throws InvalidQueryException
	 */
	public function findBackendJobs(int $recordPageId, array $filters = [], int $limit = 0, int $offset = 0) {
		$query = $this->createQuery();
		$query->setOrderings(
			[
				'sorting' => QueryInterface::ORDER_ASCENDING,
			]
		);
		$query->getQuerySettings()->setStoragePageIds([$recordPageId]);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$constraints = [];
		if (isset($filters['locations'])) {
			if (\is_array($filters['locations'])) {
				$constraints[] = $query->in('company.city', $filters['locations']);
			} elseif ($filters['locations'] !== '') {
				$constraints[] = $query->equals('company.city', $filters['locations']);
			}
		}

		if (isset($filters['search']) && $filters['search'] !== '') {
			$constraints[] = $query->like('title', '%' . $filters['search'] . '%');
		}

		if ($limit > 0) {
			$query->setLimit($limit);
		}

		if ($offset > 0) {
			$query->setOffset($offset);
		}

		if (\count($constraints) > 1) {
			return $query->matching($query->logicalAnd(...$constraints))->execute();
		}

		if (\count($constraints) > 0) {
			return $query->matching($constraints[0])->execute();
		}

		return $query->execute();
	}

	/**
	 * Returns a job filtered by company and page id
	 *
	 * @param array $jobIds
	 * @param int $limit
	 * @param int $offset
	 * @return QueryResultInterface
	 */
	public function findByJobIds(array $jobIds = [], $limit = 0, $offset = 0): ExtbaseQueryResultInterface {
		$query = $this->createQuery();
		$query->setOrderings(
			[
				'sorting' => QueryInterface::ORDER_ASCENDING,
			]
		);

		// Ignore enable fields in backend
		$querySettings = $query->getQuerySettings();
		$querySettings->setIgnoreEnableFields(TRUE);
		$storagePageIds = $query->getQuerySettings()->getStoragePageIds();
		if (empty($storagePageIds)) {
			// if no record storage page has been selected in the plugin, ignore it
			$querySettings->setRespectStoragePage(FALSE);
		}

		$this->setDefaultQuerySettings($querySettings);

		$constraints = [];

		if (\is_array($jobIds) && \count($jobIds)) {
			$companyConstraints = [];
			foreach ($jobIds as $jobId) {
				if ($jobId) {
					$companyConstraints[] = $query->equals('uid', $jobId);
				}
			}

			if (\count($companyConstraints)) {
				$constraints[] = $query->logicalOr(...$companyConstraints);
			}
		}

		if (\count($constraints)) {
			$query->matching($query->logicalAnd(...$constraints));
		}

		if ($limit > 0) {
			$query->setLimit($limit);
		}

		if ($offset > 0) {
			$query->setOffset($offset);
		}

		return $query->execute();
	}

	/**
	 * Find jobs matching the filter settings given
	 *
	 * @param array $filters
	 * @param int $limit
	 * @param int $offset
	 * @param int $ordering
	 * @param array $explicitFilters
	 * @return ExtbaseQueryResultInterface
	 * @throws Exception
	 */
	public function findJobsByFilter(
		array $filters = [],
		int $limit = 0,
		int $offset = 0,
		int $ordering = 0,
		array $explicitFilters = []
	): ExtbaseQueryResultInterface {
		$query = $this->createQuery();
		$storagePageIds = $query->getQuerySettings()->getStoragePageIds();

		// we always want to show all jobs if the strict mode is used (translated or
		// created in the specific language w/o default)
		$languageAspect = $query->getQuerySettings()->getLanguageAspect();
		if ($languageAspect->getOverlayType() === LanguageAspect::OVERLAYS_ON) {
			$newLanguageAspect = new LanguageAspect(
				$languageAspect->getId(),
				$languageAspect->getContentId(),
				LanguageAspect::OVERLAYS_OFF,
				$languageAspect->getFallbackChain()
			);
			$query->getQuerySettings()->setLanguageAspect($newLanguageAspect);
		}

		if (empty($storagePageIds)) {
			// if no record storage page has been selected in the plugin, ignore it
			$query->getQuerySettings()->setRespectStoragePage(FALSE);
		}

		if ($ordering === self::ORDER_BY_TITLE) {
			$query->setOrderings(
				[
					'title' => QueryInterface::ORDER_ASCENDING,
				]
			);
		}

		if ($ordering === self::ORDER_BY_CRDATE) {
			$query->setOrderings(
				[
					'crdate' => QueryInterface::ORDER_DESCENDING
				]
			);
		}

		if ($ordering === self::ORDER_BY_SORTING) {
			$query->setOrderings(
				[
					'sorting' => QueryInterface::ORDER_ASCENDING
				]
			);
		}

		$constraints = [];

		// the country filter is very special, because we only can match direct strings inside the company table
		// TODO countries must be moved to an own table and properly assigned to simplify the whole filtering process and to reduce many possible errors
		if (isset($filters['filterCountry']) && $filters['filterCountry'] !== '0') {
			$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
			$queryBuilder = $connectionPool->getQueryBuilderForTable('tx_sgjobs_domain_model_company');
			$result = $queryBuilder
				->select('l10n_parent')
				->from('tx_sgjobs_domain_model_company')
				->where(
					$queryBuilder->expr()->eq(
						'country',
						$queryBuilder->createNamedParameter($filters['filterCountry'])
					),
				)
				->setMaxResults(1)
				->executeQuery();
			$result = $result->fetchAllAssociative();
			if (count($result)) {
				$l10nParent = $result[0]['l10n_parent'];
				if ($l10nParent > 0) {
					$result = $queryBuilder
						->select('country')
						->from('tx_sgjobs_domain_model_company')
						->where($queryBuilder->expr()->eq('uid', $l10nParent))
						->setMaxResults(1)
						->executeQuery();
					$result = $result->fetchAllAssociative();
					if (count($result)) {
						$country = $result[0]['country'];
						if ($country !== '') {
							$filters['filterCountry'] = $country;
						}
					}
				}
			}

			$constraints[] = $query->equals('company.country', $filters['filterCountry']);
		}

		if (isset($filters['filterRemote']) && $filters['filterRemote'] !== '') {
			$constraints[] = $query->logicalAnd(
				$query->equals('telecommutePossible', TRUE),
				$query->equals('office_work_possible', FALSE)
			);
		}

		if (isset($filters['filterLocation']) && $filters['filterLocation'] !== '0') {
			$constraints[] = $query->equals('company.uid', $filters['filterLocation']);
		}

		if (isset($filters['filterByLocation'])) {
			$companyConstraints = [];
			foreach ($filters['filterByLocation'] as $companyFilter) {
				$companyConstraints[] = $query->equals('company.uid', $companyFilter);
			}

			if (\count($companyConstraints) > 0) {
				$constraints[] = $query->logicalOr(...$companyConstraints);
			}
		}

		if (isset($filters['filterDepartment']) && $filters['filterDepartment'] !== '0') {
			$constraints[] = $query->equals('department', $filters['filterDepartment']);
		} elseif (isset($filters['filterByDepartment'])) {
			$departmentConstraints = [];
			foreach ($filters['filterByDepartment'] as $departmentFilter) {
				$departmentConstraints[] = $query->equals('department', $departmentFilter);
			}

			if (\count($departmentConstraints) > 0) {
				$constraints[] = $query->logicalOr(...$departmentConstraints);
			}
		}

		if (isset($filters['filterExperienceLevel']) && $filters['filterExperienceLevel'] !== '0') {
			$constraints[] = $query->equals('experienceLevel', $filters['filterExperienceLevel']);
		} elseif (isset($filters['filterByExperienceLevel'])) {
			$experienceConstraints = [];
			foreach ($filters['filterByExperienceLevel'] as $experienceFilter) {
				$experienceConstraints[] = $query->equals('experienceLevel', $experienceFilter);
			}

			if (\count($experienceConstraints) > 0) {
				$constraints[] = $query->logicalOr(...$experienceConstraints);
			}
		}

		if (\count($constraints)) {
			$query->matching($query->logicalAnd(...$constraints));
		}

		if ($limit > 0) {
			$query->setLimit($limit);
		}

		if ($offset > 0) {
			$query->setOffset($offset);
		}

		return $query->execute();
	}

	/**
	 * Finds related jobs by company & department.
	 * Tries to find at least $minRelatedJobs by first combining all constraints via AND.
	 * If we cannot find enough related jobs with these constraints, we repeat the query but with fewer constraints.
	 * E.g. we can only find 1 record which has the same country AND department -> fill the remaining space with
	 * jobs, which have either the same country OR department.
	 *
	 * @param Job $job
	 * @param int $limit
	 * @return array
	 */
	public function findRelated(Job $job, int $limit = 0): array {
		// the minimum amount of related jobs, we should try to find before removing constraints
		$minRelatedJobs = 2;
		$relatedJobs = [];
		$query = $this->prepareRelatedJobsQuery($job, 0, $limit);

		$resultCount = $query->count();

		// fill $relatedJobs with more related jobs, by repeating the query, but with fewer constraints
		if ($resultCount < $minRelatedJobs) {
			$queryResult = $query->execute()->toArray();
			array_push($relatedJobs, ...$queryResult);
			$query = $this->prepareRelatedJobsQuery($job, 1, $limit - $resultCount);
			$queryResult = $query->execute()->toArray();
			array_push($relatedJobs, ...$queryResult);
			$relatedJobs = array_unique($relatedJobs);
			$relatedJobsCount = count($relatedJobs);
			if ($relatedJobsCount < $minRelatedJobs) {
				$query = $this->prepareRelatedJobsQuery($job, 2, $limit - $relatedJobsCount);
				$queryResult = $query->execute()->toArray();
				array_push($relatedJobs, ...$queryResult);
				$relatedJobs = array_unique($relatedJobs);
			}
		} else {
			$relatedJobs = $query->execute()->toArray();
		}

		return $relatedJobs;
	}

	/**
	 * Helper function, to avoid duplicate code.
	 * Returns a query, to be used within findRelated()
	 *
	 * @param Job $job
	 * @param int $iteration used different conditions based on the iteration step
	 * @param int $limit
	 * @return QueryInterface
	 */
	protected function prepareRelatedJobsQuery(Job $job, int $iteration, int $limit = 0): QueryInterface {
		$query = $this->createQuery();
		$constraints = [];
		$storagePageIds = $query->getQuerySettings()->getStoragePageIds();
		if (empty($storagePageIds)) {
			// if no record storage page has been selected in the plugin, ignore it
			$query->getQuerySettings()->setRespectStoragePage(FALSE);
		}

		$company = $job->getFirstCompany();
		$department = $job->getDepartment();

		if ($limit > 0) {
			$query->setLimit($limit);
		}

		// look for jobs, which have the same company AND department
		if ($iteration === 0) {
			if ($company !== NULL) {
				$constraints[] = $query->equals('company', $company->getUid());
			}

			if ($department !== NULL) {
				$constraints[] = $query->equals('department', $department->getUid());
			}
		} elseif (($iteration === 1) && $company !== NULL) {
			// look for jobs, which have the same company
			$constraints[] = $query->equals('company', $company->getUid());
		} elseif (($iteration === 2) && $department !== NULL) {
			// look for jobs, which have the same department
			$constraints[] = $query->equals('department', $department->getUid());
		}

		if (\count($constraints)) {
			$query->matching($query->logicalAnd(...$constraints));
		}

		return $query;
	}

	/**
	 * Gets the amount of jobs with filters
	 *
	 * @param array $filters
	 * @return int
	 */
	public function countAll(array $filters = []): int {
		$query = $this->createQuery();

		$storagePageIds = $query->getQuerySettings()->getStoragePageIds();
		if (empty($storagePageIds)) {
			// if no record storage page has been selected in the plugin, ignore it
			$query->getQuerySettings()->setRespectStoragePage(FALSE);
		}

		if (count($filters) > 0) {
			$constraints = $this->getFilterConstraints($query, $filters);
			$query->matching($query->logicalAnd(...$constraints));
		}

		return $query->execute()->count();
	}

	/**
	 * Gets the featured jobs filtered
	 *
	 * @param array $filters
	 * @param int $limit
	 * @return array|ExtbaseQueryResultInterface
	 */
	public function findByFeaturedOffer(array $filters = [], int $limit = 3) {
		$query = $this->createQuery();

		$storagePageIds = $query->getQuerySettings()->getStoragePageIds();
		if (count($storagePageIds) <= 0) {
			// if no record storage page has been selected in the plugin, ignore it
			$query->getQuerySettings()->setRespectStoragePage(FALSE);
		}

		if (count($filters) > 0) {
			$constraints = $this->getFilterConstraints($query, $filters);
			$query->matching($query->logicalAnd(...$constraints));
		}

		$query->setOrderings(['featured_offer' => QueryInterface::ORDER_DESCENDING]);
		$query->setLimit($limit);
		return $query->execute();
	}

	/**
	 * Creates the query contraints used for general filtering
	 *
	 * @param $query
	 * @param $filters
	 * @return array
	 */
	protected function getFilterConstraints($query, $filters): array {
		$constraints = [];

		if (isset($filters['filterByLocation'])) {
			$companyConstraints = [];
			foreach ($filters['filterByLocation'] as $companyFilter) {
				$companyConstraints[] = $query->equals('company.uid', $companyFilter);
			}

			if (\count($companyConstraints) > 0) {
				$constraints[] = $query->logicalOr(...$companyConstraints);
			}
		}

		if (isset($filters['filterByDepartment'])) {
			$departmentConstraints = [];
			foreach ($filters['filterByDepartment'] as $departmentFilter) {
				$departmentConstraints[] = $query->equals('department', $departmentFilter);
			}

			if (\count($departmentConstraints) > 0) {
				$constraints[] = $query->logicalOr(...$departmentConstraints);
			}
		}

		if (isset($filters['filterByExperienceLevel'])) {
			$experienceConstraints = [];
			foreach ($filters['filterByExperienceLevel'] as $experienceFilter) {
				$experienceConstraints[] = $query->equals('experienceLevel', $experienceFilter);
			}

			if (\count($experienceConstraints) > 0) {
				$constraints[] = $query->logicalOr(...$experienceConstraints);
			}
		}

		return $constraints;
	}
}
