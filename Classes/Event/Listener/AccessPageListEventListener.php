<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Event\Listener;

use SGalinski\SgJobs\Service\SitemapService;
use SGalinski\SgSeo\Events\AccessPageListEvent;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * Class AccessPageListEventListener
 *
 * @package SGalinski\SgJobs\Event\Listener
 */
class AccessPageListEventListener {
	/**
	 * @var SitemapService
	 */
	protected $sitemapService;

	/**
	 * AccessPageListEventListener constructor.
	 *
	 * @param SitemapService $sitemapService
	 */
	public function __construct(SitemapService $sitemapService) {
		$this->sitemapService = $sitemapService;
	}

	/**
	 * @param AccessPageListEvent $event
	 * @throws AspectNotFoundException
	 */
	public function __invoke(AccessPageListEvent $event) {
		$pageList = $event->getPageList();
		$additionalPageList = $this->sitemapService->generatePagesList(
			$event->getSysLanguageUid(),
			$event->getSite()->getRootPageId()
		);

		ArrayUtility::mergeRecursiveWithOverrule($pageList, $additionalPageList);
		$event->setPageList($pageList);
	}
}
