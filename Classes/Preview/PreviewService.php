<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Preview;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * Previewservice to return the view
 */
class PreviewService {
	public const RETURNTYPE_ARR = 'array';

	/**
	 * Returns an application view for previews
	 *
	 * @param array $row
	 * @return StandaloneView
	 */
	public function getApplicationView(array $row): StandaloneView {
		$view = $this->createView();
		$view->assign('uid', $row['uid']);

		// Get available plugin settings and their values from flexform
		$pluginConfiguration = GeneralUtility::xml2array(
			$row['pi_flexform'],
			'T3DataStructure'
		)['data']['sDEF']['lDEF'];

		$redirectPage = $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.redirectPage');
		$templateData = [
			'redirectPage' => is_string($redirectPage) ? $this->addFieldContentsToRecordIdList(
				'pages',
				$redirectPage
			) : '',
			'privacyPolicyPage' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.privacyPolicyPage')
		];

		$view->assign('data', $templateData);
		$view->assign('headerLabel', BackendUtility::getLabelFromItemListMerged(
			$row['pid'],
			'tt_content',
			'list_type',
			$row['list_type'],
			$row
		));
		return $view;
	}

	/**
	 * returns joblistview for previews
	 *
	 * @param array $row
	 * @return StandaloneView
	 */
	public function getJoblistView(array $row): StandaloneView {
		$view = $this->createView();
		$view->assign('uid', $row['uid']);

		// Get available plugin settings and their values from flexform
		$pluginConfiguration = GeneralUtility::xml2array(
			$row['pi_flexform'],
			'T3DataStructure'
		)['data']['sDEF']['lDEF'];

		$applyPage = $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.applyPage');
		$filterByExperienceLevel = $this->passVDefOnKeyToTemplate(
			$pluginConfiguration,
			'settings.filterByExperienceLevel'
		);
		$templateData = [
			'applyPage' => is_string($applyPage) ? $this->addFieldContentsToRecordIdList(
				'pages',
				$applyPage
			) : '',
			'jobLimit' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.jobLimit'),
			'orderBy' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.orderBy'),
			'filterByExperienceLevel' => is_string(
				$filterByExperienceLevel
			) ? $this->addFieldContentsToRecordIdList(
				'tx_sgjobs_domain_model_experience_level',
				$filterByExperienceLevel
			) : ''
		];

		$view->assign('data', $templateData);

		// Also assign the standard plugin settings to the template
		// as they are actually used in the corresponding controller action.
		$view->assign(
			'storagePages',
			is_string($row['pages']) ? $this->addFieldContentsToRecordIdList(
				'pages',
				$row['pages']
			) : ''
		);
		$view->assign('recursive', $row['recursive']);
		$view->assign('headerLabel', BackendUtility::getLabelFromItemListMerged(
			$row['pid'],
			'tt_content',
			'list_type',
			$row['list_type'],
			$row
		));
		return $view;
	}

	public function getTeaserView(array $row): StandaloneView {
		$view = $this->createView();
		$view->assign('uid', $row['uid']);

		// Get available plugin settings and their values from flexform
		$pluginConfiguration = GeneralUtility::xml2array(
			$row['pi_flexform'],
			'T3DataStructure'
		)['data']['sDEF']['lDEF'];

		$offersPage = $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.offersPage');
		$applyPage = $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.applyPage');
		$locations = $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.locations');
		$templateData = [
			'offersPage' => is_string($offersPage) ? $this->addFieldContentsToRecordIdList(
				'pages',
				$offersPage
			) : '',
			'applyPage' => is_string($applyPage) ? $this->addFieldContentsToRecordIdList(
				'pages',
				$applyPage
			) : '',
			'locations' => is_string($locations) ? $this->addFieldContentsToRecordIdList(
				'tx_sgjobs_domain_model_company',
				$locations,
				'name'
			) : '',
		];

		$view->assign('data', $templateData);
		$view->assign('headerLabel', BackendUtility::getLabelFromItemListMerged(
			$row['pid'],
			'tt_content',
			'list_type',
			$row['list_type'],
			$row
		));
		return $view;
	}

	/**
	 * @param array $conf
	 * @param string $key
	 * @param string $returnType
	 * @return array|mixed|string
	 */
	private function passVDefOnKeyToTemplate(array $conf, string $key, string $returnType = '') {
		if (isset($conf[$key])) {
			return $conf[$key]['vDEF'];
		}

		// check if we got a possible returntype:
		if ($returnType === self::RETURNTYPE_ARR) {
			return [];
		}

		return '';
	}

	/**
	 * Creates a new StandaloneView object with template and partial root paths,
	 * attaches the template with the given name to it and returns it.
	 *
	 * @return StandaloneView
	 */
	protected function createView(): StandaloneView {
		$view = GeneralUtility::makeInstance(StandaloneView::class);
		$view->setTemplateRootPaths(['EXT:sg_jobs/Resources/Private/Backend/Templates/']);
		$view->setPartialRootPaths(['EXT:sg_jobs/Resources/Private/Backend/Partials/']);
		$view->setLayoutRootPaths(['EXT:sg_jobs/Resources/Private/Backend/Layouts/']);
		return $view;
	}

	/**
	 * Takes a comma-separated list of record IDs, the corresponding table and optionally the field to look up.
	 * Returns another comma-space-separated list of the same records with the content of the field added.
	 * The returned list should look nice enough to be rendered in the backend preview directly.
	 *
	 * @param string $table
	 * @param string $recordIdList
	 * @param string $field
	 * @return string
	 */
	protected function addFieldContentsToRecordIdList(
		string $table,
		string $recordIdList,
		string $field = 'title'
	): string {
		$recordIdsArray = GeneralUtility::intExplode(',', $recordIdList, TRUE);
		$recordsWithTitlesArray = [];

		foreach ($recordIdsArray as $recordId) {
			$record = BackendUtility::getRecord($table, $recordId, $field);

			if (!$record) {
				continue;
			}

			$recordsWithTitlesArray[] = $record[$field] . ' [' . $recordId . ']';
		}

		return implode(', ', $recordsWithTitlesArray);
	}
}
