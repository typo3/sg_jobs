<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\Service;

use TYPO3\CMS\Core\Charset\CharsetConverter;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Resource\Exception\ExistingTargetFolderException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderWritePermissionsException;
use TYPO3\CMS\Core\Resource\File as FalFile;
use TYPO3\CMS\Core\Resource\FileReference as FalFileReference;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\File\BasicFileUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Service for diverse file operations
 */
class FileAndFolderService implements SingletonInterface {
	/**
	 *
	 * @var BasicFileUtility
	 */
	protected $fileUtility;

	/**
	 * Inject the FileUtility
	 *
	 * @param BasicFileUtility $fileUtility
	 */
	public function injectFileUtility(BasicFileUtility $fileUtility) {
		$this->fileUtility = $fileUtility;
	}

	/**
	 * @var ResourceFactory
	 *
	 */
	protected $resourceFactory;

	/**
	 * Inject the ResourceFactory
	 *
	 * @param ResourceFactory $resourceFactory
	 */
	public function injectResourceFactory(ResourceFactory $resourceFactory) {
		$this->resourceFactory = $resourceFactory;
	}

	/**
	 *
	 * @var StorageRepository
	 */
	protected $storageRepository;

	/**
	 * Inject the StorageRepository
	 *
	 * @param StorageRepository $storageRepository
	 */
	public function injectStorageRepository(StorageRepository $storageRepository) {
		$this->storageRepository = $storageRepository;
	}

	/**
	 *
	 * @var CharsetConverter
	 */
	protected $characterSetConverter;

	/**
	 * Inject the CharacterSetConverter
	 *
	 * @param CharsetConverter $characterSetConverter
	 */
	public function injectCharacterSetConverter(CharsetConverter $characterSetConverter) {
		$this->characterSetConverter = $characterSetConverter;
	}

	/**
	 * @var PersistenceManager
	 *
	 */
	protected $persistenceManager;

	/**
	 * Inject the PersistenceManager
	 *
	 * @param PersistenceManager $persistenceManager
	 */
	public function injectPersistenceManager(PersistenceManager $persistenceManager) {
		$this->persistenceManager = $persistenceManager;
	}

	/**
	 * @var array
	 */
	protected $allowedFileExtensionCache = [];

	/**
	 * @var array
	 */
	protected $settings = [];

	/**
	 * Method for uploading a file.
	 *
	 * @param string $temporaryFileName
	 * @param int $size
	 * @return string
	 */
	public function uploadFile($temporaryFileName, $size): string {
		if ($size <= 0) {
			return '';
		}

		$newFile = GeneralUtility::upload_to_tempfile($temporaryFileName);
		GeneralUtility::fixPermissions($newFile);
		return \basename($newFile);
	}

	/**
	 * Method for clean typo3temp from unused temporary files.
	 *
	 * @param string $name
	 * @return boolean
	 */
	public function removeTemporaryFile($name): bool {
		$fileRemoved = FALSE;
		if (\trim($name) !== '') {
			$name = GeneralUtility::getFileAbsFileName('typo3temp/' . $name);
			$fileRemoved = (bool) GeneralUtility::unlink_tempfile($name);
		}

		return $fileRemoved;
	}

	/**
	 * This method creates the fal folder.
	 *
	 * @param string $folderIdentifier
	 * @param int $storageUid
	 * @return Folder
	 * @throws \InvalidArgumentException
	 * @throws InsufficientFolderWritePermissionsException
	 * @throws InsufficientFolderAccessPermissionsException
	 * @throws ExistingTargetFolderException
	 * @throws \Exception
	 */
	protected function createFolder($folderIdentifier, $storageUid): Folder {
		return $this->getStorage($storageUid)->createFolder($folderIdentifier);
	}

	/**
	 * Returns a storage instance.
	 *
	 * @param int $storageUid
	 * @return ResourceStorage
	 */
	public function getStorage($storageUid = 1): ResourceStorage {
		/** @var ResourceStorage $storage */
		$storage = $this->storageRepository->findByUid($storageUid);
		if (Environment::isCli() && $storage !== NULL) {
			// if not set, the storage cannot access any folder in CLI mode (TYPO3 bug?)
			$storage->setEvaluatePermissions(FALSE);
		}

		return $storage;
	}

	/**
	 * Returns an array with allowed file extensions.
	 *
	 * The types array can contain e.g. image or document and will be configured at
	 * plugin.tx_rsevents.settings.uploader in the typoscript.
	 *
	 * @param array $types
	 * @return array
	 */
	public function getAllowedFileExtensions(array $types = []): array {
		$fileTypes = '';
		$key = \md5(\implode('_', $types));

		if (!isset($this->allowedFileExtensionCache[$key])) {
			foreach ($types as $type) {
				$type = \lcfirst($type . 'FileExtensions');

				if (!\array_key_exists($type, $this->settings['rsUploader'])) {
					continue;
				}

				if ($fileTypes !== '' && !str_ends_with($fileTypes, ',')) {
					$fileTypes .= ',';
				}

				$fileTypes .= $this->settings['uploader'][$type];
			}

			$this->allowedFileExtensionCache[$key] = GeneralUtility::trimExplode(',', $fileTypes, TRUE);
		}

		return $this->allowedFileExtensionCache[$key];
	}

	/**
	 * Method create FAL records from the temporary files of the new impression. The new fileReferences will
	 * be attached to the new impression.
	 *
	 * @param array $fileInfo
	 * @param Folder $folder
	 * @return FalFile
	 */
	public function createFileRecords(array $fileInfo, Folder $folder): FalFile {
		return $folder->addUploadedFile($fileInfo);
	}

	/**
	 * Returns the folder for the given folder identifier.
	 *
	 * @param string $folderIdentifier
	 * @return Folder
	 * @throws InsufficientFolderWritePermissionsException
	 * @throws \InvalidArgumentException
	 * @throws \Exception
	 * @throws InsufficientFolderAccessPermissionsException
	 * @throws ExistingTargetFolderException
	 */
	public function getFolder($folderIdentifier): Folder {
		$storage = $this->getStorage();

		if ($storage->hasFolder($folderIdentifier)) {
			return $storage->getFolder($folderIdentifier);
		}

		/** @var Folder $folder */
		$folder = $storage->getFolder('sg_comments');
		$this->createHtaccessFile($folder);
		return $storage->createFolder($folderIdentifier);
	}

	/**
	 * Write a htaccess file to the folder with the appropriate settings
	 *
	 * @param Folder $folder
	 */
	private function createHtaccessFile($folder) {
		$path = $folder->getPublicUrl();
		$file = $path . '.htaccess';
		$absolutePublicPath = Environment::getPublicPath();
		$file = $absolutePublicPath . $file;

		if (!\file_exists($file)) {
			$current = '';

			$allowedImageFileExtensions = 'png|gif|bmp|ico|jpeg|jpg|ps|psd|svg|tif|tiff|dds|pspimage|tga|';
			$allowedVideoFileExtensions = '3g2|3gp|avi|flv|h264|m4v|mkv|mov|mp4|mpg|mpeg|rm|swf|vob|wmv|';
			$allowedAudioFileExtensions = 'aif|cda|mid|midi|mp3|mpa|ogg|wav|wma|wpl';

			$current .= <<<EOT
order allow,deny
<Files ~ "\.($allowedImageFileExtensions$allowedVideoFileExtensions$allowedAudioFileExtensions)$">
   allow from all
</Files>
EOT;

			\file_put_contents($file, $current);
		}
	}

	/**
	 * Method the returns the given subfolder. The FAL core method throws an exception if the folder does not exists.
	 * And this method just create the folder.
	 *
	 * @param Folder $folder
	 * @param string $subfolderName
	 * @return Folder
	 * @throws \InvalidArgumentException
	 */
	public function getSubfolder(Folder $folder, $subfolderName): Folder {
		if ($folder->hasFolder($subfolderName)) {
			return $folder->getSubfolder($subfolderName);
		}

		return $folder->createFolder($subfolderName);
	}

	/**
	 * This creates a fal file reference object.
	 *
	 * @param FalFile $file
	 * @param int $resourcePointer
	 * @return FileReference
	 */
	public function createFileReferenceFromFalFileObject(FalFile $file, $resourcePointer = NULL): FileReference {
		$fileReference = $this->resourceFactory->createFileReferenceObject(
			[
				'uid_local' => $file->getUid(),
				'uid_foreign' => \uniqid('NEW_', TRUE),
				'uid' => \uniqid('NEW_', TRUE),
			]
		);

		return $this->createFileReferenceFromFalFileReferenceObject($fileReference, $resourcePointer);
	}

	/**
	 * This creates a file reference extbase object from a given fal file reference object.
	 *
	 * @param FalFileReference $falFileReference
	 * @param int $resourcePointer
	 * @return FileReference
	 */
	protected function createFileReferenceFromFalFileReferenceObject(
		FalFileReference $falFileReference,
		$resourcePointer = NULL
	): FileReference {
		if ($resourcePointer === NULL) {
			/** @var FileReference $fileReference */
			$fileReference = GeneralUtility::makeInstance(FileReference::class);
		} else {
			$fileReference = $this->persistenceManager->getObjectByIdentifier(
				$resourcePointer,
				FileReference::class
			);
		}

		$fileReference->setOriginalResource($falFileReference);

		return $fileReference;
	}

	/**
	 * Generate thumbnail image for the given file. This function creates a sub folder for the thumbnails if this is
	 * needed and attaches the processed new thumbnail file to this folder.
	 *
	 * @param FalFile $file
	 * @param Folder $folder
	 * @param array $uploadSettings
	 * @return FalFile
	 * @throws \InvalidArgumentException
	 */
	public function generateFileUploadThumbnail(FalFile $file, Folder $folder, array $uploadSettings): FalFile {
		$thumbnailSettings = $uploadSettings['thumbnail'];
		$thumbnailFolder = $this->getThumbnailFolder($folder, $thumbnailSettings);
		$thumbnailFileName = $this->getThumbnailFileName($file, $thumbnailSettings);
		$thumbnailConfig = [
			'width' => $thumbnailSettings['resizeSettings']['width'],
			'height' => $thumbnailSettings['resizeSettings']['height'],
			'additionalParameters' => '-quality ' . $thumbnailSettings['resizeSettings']['quality']
		];

		return $thumbnailFolder->addFile(
			$file->process(ProcessedFile::CONTEXT_IMAGECROPSCALEMASK, $thumbnailConfig)->getPublicUrl(),
			$thumbnailFileName,
			'replace'
		);
	}

	/**
	 * Returns name of a generated thumbnail.
	 *
	 * @param FalFile $file
	 * @param array $thumbnailSettings
	 * @return string
	 */
	public function getThumbnailFileName(FalFile $file, array $thumbnailSettings): string {
		$thumbnailFileName = $file->getNameWithoutExtension() . '_' . $thumbnailSettings['prefix'];
		$thumbnailFileName .= '.' . $file->getExtension();

		return $thumbnailFileName;
	}

	/**
	 * Returns the thumbnail sub folder and creates the folder if no folder exists.
	 *
	 * @param Folder $folder
	 * @param array $thumbnailSettings
	 * @return Folder
	 * @throws \InvalidArgumentException
	 */
	public function getThumbnailFolder(Folder $folder, array $thumbnailSettings): Folder {
		return $this->getSubfolder($folder, $thumbnailSettings['prefix']);
	}
}
