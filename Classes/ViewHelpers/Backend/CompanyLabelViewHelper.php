<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\ViewHelpers\Backend;

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class CompanyLabelViewHelper
 **/
class CompanyLabelViewHelper extends AbstractViewHelper {
	/**
	 * Initialize the ViewHelper arguments
	 */
	public function initializeArguments(): void {
		parent::initializeArguments();
		$this->registerArgument('uid', 'int', 'The uid of the Job', TRUE);
		$this->registerArgument('companyraw', 'string', 'The raw company field of the job', TRUE);
	}

	/**
	 * Renders the control buttons for the specified record
	 *
	 * @return string
	 * @throws Exception
	 */
	public function render(): string {
		$addedCompanies = explode(',', $this->arguments['companyraw']);
		$queryBilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			'tx_sgjobs_domain_model_company'
		);
		$firstJob = $queryBilder->select('name', 'city')
			->from('tx_sgjobs_domain_model_company')
			->where(
				$queryBilder->expr()->in('uid', $addedCompanies)
			)
			->setMaxResults(1)
			->executeQuery()
			->fetchAssociative();

		return $firstJob ? $firstJob['name'] . ', ' . $firstJob['city'] : '';
	}
}
