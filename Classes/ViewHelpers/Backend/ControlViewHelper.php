<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\ViewHelpers\Backend;

use SGalinski\SgJobs\Domain\Model\Job;
use TYPO3\CMS\Backend\RecordList\DatabaseRecordList;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Type\Bitmask\Permission;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class ControlViewHelper
 **/
class ControlViewHelper extends AbstractViewHelper {
	/**
	 * Initialize the ViewHelper arguments
	 */
	public function initializeArguments(): void {
		parent::initializeArguments();
		$this->registerArgument('table', 'string', 'The table to control', TRUE);
		$this->registerArgument('row', 'array', 'The row of the record', TRUE);
		$this->registerArgument('allItems', 'array', 'allRows', FALSE);
		$this->registerArgument('iter', 'array', 'current Iterator', FALSE);
	}

	/**
	 * Renders the control buttons for the specified record
	 *
	 * @return string
	 */
	public function render(): string {
		$table = $this->arguments['table'];
		/** @var Job $row */
		$row = $this->arguments['row'];
		$row = BackendUtility::getRecord('tx_sgjobs_domain_model_job', $row['uid']);

		/** @var DatabaseRecordList $databaseRecordList */
		$databaseRecordList = GeneralUtility::makeInstance(DatabaseRecordList::class);
		$databaseRecordList->setRequest($this->renderingContext->getRequest());
		$pageInfo = BackendUtility::readPageAccess($row['pid'], $GLOBALS['BE_USER']->getPagePermsClause(1));
		$databaseRecordList->calcPerms = new Permission($GLOBALS['BE_USER']->calcPerms($pageInfo));

		// DatabaseRecordList has a $prevPrevUid, which might track back a page. We won't do that, 1st+last record are
		// not sortable in one direction
		$uidPrev = FALSE;
		$uidNext = FALSE;
		$iterator = $this->arguments['iter'];
		if (!$iterator['isFirst'] && $iterator['index'] !== 1) {
			$uidPrev = '-' . $this->arguments['allItems'][$iterator['index'] - 2]['uid'];
		}

		if ($iterator['index'] === 1) {
			$uidPrev = $row['pid'];
		}

		if (!$iterator['isLast']) {
			$uidNext = '-' . $this->arguments['allItems'][$iterator['index'] + 1]['uid'];
		}

		$newCurrentTableArr = [
			'prev' => [
				$row['uid'] => $uidPrev
			],
			'next' => [
				$row['uid'] => $uidNext
			]
		];
		if (!$uidPrev) {
			$newCurrentTableArr = [
				'next' => [
					$row['uid'] => $uidNext
				]
			];
		}

		if (!$uidNext) {
			$newCurrentTableArr = [
				'prev' => [
					$row['uid'] => $uidPrev
				]
			];
		}

		$databaseRecordList->id = $row['pid'];
		$databaseRecordList->currentTable = $newCurrentTableArr;

		return $databaseRecordList->makeControl($table, $row);
	}
}
