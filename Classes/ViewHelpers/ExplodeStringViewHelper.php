<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the AY project. The AY project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Takes a string containing multiple values separated by a delimiter and returns an array of these values.
 *
 * Example:
 * {namespace jobs=SGalinski\SgJobs\ViewHelpers}
 * <jobs:explodeString string="1,2,3,4" delimiter=","/>
 */
class ExplodeStringViewHelper extends AbstractViewHelper {
	/**
	 * Register the ViewHelper arguments
	 */
	public function initializeArguments(): void {
		parent::initializeArguments();
		$this->registerArgument('string', 'string', 'The string to explode', TRUE);
		$this->registerArgument('delimiter', 'string', 'The string separating the values in the string', TRUE);
	}

	/**
	 * Returns the exploded array of the given string value, separated by the delimiter
	 *
	 * @return array
	 */
	public function render(): array {
		return GeneralUtility::trimExplode($this->arguments['delimiter'], $this->arguments['string']);
	}
}
