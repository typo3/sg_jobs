<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Helmut Hummel <helmut.hummel@typo3.org>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the text file GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\ViewHelpers\Form;

use TYPO3\CMS\Extbase\Property\PropertyMapper;
use TYPO3\CMS\Extbase\Security\Cryptography\HashService;
use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormFieldViewHelper;

/**
 * Class UploadViewHelper
 */
class UploadViewHelper extends AbstractFormFieldViewHelper {
	/**
	 * @var HashService
	 *
	 */
	protected $hashService;

	/**
	 * @var PropertyMapper
	 *
	 */
	protected $propertyMapper;

	/**
	 * @var string
	 */
	protected $tagName = 'input';

	/**
	 * Inject the HashService
	 *
	 * @param HashService $hashService
	 */
	public function injectHashService(HashService $hashService) {
		$this->hashService = $hashService;
	}

	/**
	 * Inject the PropertyMapper
	 *
	 * @param PropertyMapper $propertyMapper
	 */
	public function injectPropertyMapper(PropertyMapper $propertyMapper) {
		$this->propertyMapper = $propertyMapper;
	}

	/**
	 * Register the ViewHelper arguments
	 */
	public function initializeArguments(): void {
		parent::initializeArguments();
		$this->registerTagAttribute('disabled', 'string', 'Specifies that the input element should be disabled when the page loads');
		$this->registerTagAttribute('multiple', 'string', 'Specifies that the file input element should allow multiple selection of files');
		$this->registerTagAttribute('accept', 'string', 'Specifies the allowed file extensions to upload via comma-separated list, example ".png,.gif"');
		$this->registerArgument('errorClass', 'string', 'CSS class to set if there are errors for this ViewHelper', FALSE, 'f3-form-error');
		$this->registerUniversalTagAttributes();
		$this->registerArgument('resourceName', 'string', 'The name of the resource', FALSE, 'resource');
	}

	/**
	 * Render the upload field including possible resource pointer
	 *
	 * @return string
	 * @throws \InvalidArgumentException
	 * @api
	 */
	public function render(): string {
		$output = '';

		$resource = $this->getUploadedResource();
		if (\is_array($resource) && $resource['name'] === '' && isset($resource['submittedFile'])) {
			$resource = $resource['submittedFile'];
		}

		if ($resource !== NULL) {
			$this->templateVariableContainer->add($this->arguments['resourceName'], $resource);
			$output .= $this->renderChildren();
		}

		$name = $this->getName();
		$allowedFields = ['name', 'type', 'tmp_name', 'error', 'size'];
		foreach ($allowedFields as $fieldName) {
			$this->registerFieldNameForFormTokenGeneration($name . '[' . $fieldName . ']');
		}
		$this->tag->addAttribute('type', 'file');

		if (isset($this->arguments['multiple'])) {
			$this->tag->addAttribute('name', $name . '[]');
		} else {
			$this->tag->addAttribute('name', $name);
		}

		$this->setErrorClassAttribute();
		$output .= $this->tag->render();
		return $output;
	}

	/**
	 * Return a previously uploaded resource.
	 * Return NULL if errors occurred during property mapping for this property.
	 *
	 * @return array|NULL
	 */
	protected function getUploadedResource() {
		if ($this->getMappingResultsForProperty()->hasErrors()) {
			return NULL;
		}

		$this->respectSubmittedDataValue = TRUE;
		return $this->getValueAttribute();
	}
}
