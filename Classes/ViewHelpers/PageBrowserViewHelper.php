<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgJobs\ViewHelpers;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * View helper that renders a page browser based upon the pagebrowse extension.
 *
 * Example:
 * {namespace sg=SGalinski\SgNews\ViewHelpers}
 * <sg:pageBrowser numberOfPages="" />
 */
class PageBrowserViewHelper extends AbstractViewHelper {
	/**
	 * Specifies whether the escaping interceptors should be disabled or enabled for the render-result of this ViewHelper
	 *
	 * @see isOutputEscapingEnabled()
	 *
	 * @var boolean
	 */
	protected $escapeOutput = FALSE;

	/**
	 * Initialize the ViewHelpers arguments
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('numberOfPages', 'int', 'The number of pages to browse', TRUE);
	}

	/**
	 * Render method of the view helper.
	 *
	 * @return string
	 * @throws \UnexpectedValueException
	 */
	public function render(): string {
		/** @var TypoScriptService $typoScriptService */
		$typoScriptService = GeneralUtility::makeInstance(TypoScriptService::class);
		$configuration = $typoScriptService->convertTypoScriptArrayToPlainArray(
			$GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_sgjobs.']
		);
		$view = GeneralUtility::makeInstance(StandaloneView::class);
		$view->setRenderingContext($this->renderingContext);
		$view->setTemplateRootPaths($configuration['view']['templateRootPaths']);
		$view->setPartialRootPaths($configuration['view']['partialRootPaths']);
		$view->setLayoutRootPaths($configuration['view']['layoutRootPaths']);
		$view->setTemplate('PageBrowser/Index');

		$currentPage = 0;
		/** @var ServerRequestInterface $request */
		$request = $GLOBALS['TYPO3_REQUEST'];
		$pageBrowserVars = $request->getQueryParams()['tx_sgjobs_pagebrowser'] ?? [];
		if (isset($pageBrowserVars['currentPage']) && (int) $pageBrowserVars['currentPage'] > 0) {
			$currentPage = (int) $pageBrowserVars['currentPage'];
		}

		$pageLinks = [];
		$start = \max($currentPage - 2, 0);
		$end = \min($this->arguments['numberOfPages'], $currentPage + 2);
		for ($i = $start; $i < $end; $i++) {
			$pageLinks[] = [
				'number' => $i + 1,
				'page' => $i,
				'isCurrentPage' => $i === $currentPage,
			];
		}

		$view->assignMultiple(
			[
				'enableLessPages' => 1,
				'enableMorePages' => 1,
				'pageLinks' => $pageLinks,
				'currentPage' => $currentPage,
				'prevPageExist' => $currentPage >= 1,
				'showLessPages' => ($currentPage - 1) > 1,
				'showNextPages' => ($currentPage + 2) < $this->arguments['numberOfPages'],
				'nextPageExist' => $currentPage < $this->arguments['numberOfPages'] - 1,
				'numberOfPages' => $this->arguments['numberOfPages']
			]
		);
		return $view->render();
	}
}
