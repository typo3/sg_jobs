<?php

use SGalinski\SgJobs\Controller\BackendController;

return [
	'web_SgJobs_backend' => [
		'parent' => 'web',
		'position' => ['after' => ''],
		'access' => 'user',
		'workspaces' => 'live',
		'path' => '/module/web/sg_jobs_backend',
		'labels' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_backend.xlf',
		'iconIdentifier' => 'extension-sg_jobs-module',
		'extensionName' => 'SgJobs',
		'controllerActions' => [
			BackendController::class => ['index']
		]
	]
];
