<?php

declare(strict_types=1);

// if you need to change this, keep in mind the changes need to be done in
// Configuration/TypoScript/Common/setup.typoscript for TYPO3 9, too
use SGalinski\SgJobs\Domain\Model\ExperienceLevel;
use SGalinski\SgJobs\Domain\Model\FileReference;
use SGalinski\SgJobs\Domain\Model\JobApplication;

return [
	FileReference::class => [
		'tableName' => 'sys_file_reference'
	],
	JobApplication::class => [
		'tableName' => 'tx_sgjobs_domain_model_job_application'
	],
	ExperienceLevel::class => [
		'tableName' => 'tx_sgjobs_domain_model_experience_level'
	]
];
