<?php

/**
 * Important! Do not return a variable named $icons, because it will result in an error.
 * The core requires this file and then the variable names will clash.
 * Either use a closure here, or do not call your variable $icons.
 */

use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;

$iconList = [];
$sgJobsIcons = [
	'extension-sg_jobs-module' => 'module-sgjobs.svg',
];

foreach ($sgJobsIcons as $identifier => $path) {
	$iconList[$identifier] = [
		'provider' => SvgIconProvider::class,
		'source' => 'EXT:sg_jobs/Resources/Public/Icons/' . $path,
	];
}

return $iconList;
