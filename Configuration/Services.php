<?php

use SGalinski\SgJobs\Event\Listener\AccessPageListEventListener;
use SGalinski\SgJobs\Event\Listener\ExcludeFromReferenceIndexListener;
use SGalinski\SgJobs\EventListeners\PageContentPreviewRenderingEventListener;
use SGalinski\SgJobs\Service\BackendService;
use SGalinski\SgJobs\Service\SitemapService;
use SGalinski\SgSeo\Events\AccessPageListEvent;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent;
use TYPO3\CMS\Core\DataHandling\Event\IsTableExcludedFromReferenceIndexEvent;

return static function (ContainerConfigurator $containerConfigurator): void {
	$services = $containerConfigurator->services();
	$services->defaults()
		->private()
		->autowire()
		->autoconfigure();
	$services->load('SGalinski\\SgJobs\\', __DIR__ . '/../Classes/');

	$services->set(SitemapService::class)->public();
	$services->set(BackendService::class)->public();

	if (class_exists(AccessPageListEvent::class)) {
		$services->set(AccessPageListEventListener::class)
			->tag('event.listener', [
				'identifier' => 'accessPageListEvent',
				'event' => AccessPageListEvent::class
			]);
	}

	$services->set(ExcludeFromReferenceIndexListener::class)
		->tag('event.listener', [
			'identifier' => 'sg-jobs/excludeFromRefIndex',
			'event' => IsTableExcludedFromReferenceIndexEvent::class
		]);

	$services->set(PageContentPreviewRenderingEventListener::class)
		->tag('event.listener', [
			'event' => PageContentPreviewRenderingEvent::class
		]);
};
