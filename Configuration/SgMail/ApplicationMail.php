<?php

use SGalinski\SgMail\Service\MailTemplateService;

return [
	'extension_key' => 'sg_jobs',
	'template_key' => 'application_mail',
	'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:mail.application.description',
	'subject' => 'Eine neue Bewerbung',
	'templateContent' => <<<EOT
Eine neue {f:if(condition: '{application.jobTitle}', then: 'Bewerbung als <b>{application.jobTitle}</b> für den Standort <b>{application.firstCompany.city}</b>.', else: 'Initiativbewerbung für den Standort <b>{application.firstCompany.city}</b>.')}

Geschlecht: {f:translate(key: 'frontend.apply.gender.{application.gender}', extensionName: 'SgJobs')}

Vorname: {application.firstName}

Nachname: {application.lastName}

Geburtsdatum: {application.birthDate}

Straße: {application.street}

Postleitzahl: {application.zip}

Ort: {application.city}

Land: {application.country}

Nationalität: {application.nationality}

Telefon: {application.phone}

Mobil: {application.mobile}

E-Mail-Adresse: {application.email}

Höchster Bildungsstand: {application.education}

Nachricht:

{application.message}

{f:if(condition: '{application.privacyPolicy}', then: 'Die Datenschutzvereinbarung wurde akzeptiert.')}
EOT
	,
	'markers' => [
		[
			'marker' => 'application.jobTitle',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'Webdeveloper',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.jobtitle'
		],
		[
			'marker' => 'application.firstCompany.location',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'München',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.location'
		],
		[
			'marker' => 'application.gender',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => '0',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.gender'
		],
		[
			'marker' => 'application.firstName',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'Max',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.firstname'
		],
		[
			'marker' => 'application.lastName',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'Mustermann',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.lastname'
		],
		[
			'marker' => 'application.street',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'Musterstraße 16',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.street'
		],
		[
			'marker' => 'application.zip',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => '99999',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.zip'
		],
		[
			'marker' => 'application.city',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'München',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.city'
		],
		[
			'marker' => 'application.country',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'Deutschland',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.country'
		],
		[
			'marker' => 'application.nationality',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'Deutschland',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.nationality'
		],
		[
			'marker' => 'application.education',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'Bachelor',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.education'
		],
		[
			'marker' => 'application.birthDate',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => '01.01.1983',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.birthdate'
		],
		[
			'marker' => 'application.phone',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => '099-555 555',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.phone'
		],
		[
			'marker' => 'application.mobile',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => '0199 - 55 5551',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.mobile'
		],
		[
			'marker' => 'application.email',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'max.mustermann@example.org',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.email'
		],
		[
			'marker' => 'application.message',
			'type' => MailTemplateService::MARKER_TYPE_STRING,
			'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore...',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.message'
		],
		[
			'marker' => 'application.freetextField1',
			'type' => \SGalinski\SgMail\Service\MailTemplateService::MARKER_TYPE_STRING,
			'value' => '',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.freetextField1'
		],
		[
			'marker' => 'application.freetextField2',
			'type' => \SGalinski\SgMail\Service\MailTemplateService::MARKER_TYPE_STRING,
			'value' => '',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.freetextField2'
		],
		[
			'marker' => 'application.freetextField3',
			'type' => \SGalinski\SgMail\Service\MailTemplateService::MARKER_TYPE_STRING,
			'value' => '',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.freetextField3'
		],
		[
			'marker' => 'application.freetextField4',
			'type' => \SGalinski\SgMail\Service\MailTemplateService::MARKER_TYPE_STRING,
			'value' => '',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.freetextField4'
		],
		[
			'marker' => 'application.freetextField5',
			'type' => \SGalinski\SgMail\Service\MailTemplateService::MARKER_TYPE_STRING,
			'value' => '',
			'description' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang.xlf:application_mail.marker.freetextField5'
		],
	]
];
