<?php

// Adds the static TypoScript template.
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addStaticFile(
	'sg_jobs',
	'Configuration/TypoScript/Frontend',
	'SgJobs - Configuration'
);
