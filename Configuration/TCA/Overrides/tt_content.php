<?php

// Register frontend plugins
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

ExtensionUtility::registerPlugin(
	'sg_jobs',
	'Joblist',
	'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_backend.xlf:listPlugin'
);

// Register frontend plugins
ExtensionUtility::registerPlugin(
	'sg_jobs',
	'JobApplication',
	'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_backend.xlf:applyPlugin'
);

// Register frontend plugins
ExtensionUtility::registerPlugin(
	'sg_jobs',
	'JobTeaser',
	'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_backend.xlf:teaserPlugin'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['sgjobs_joblist'] = 'pi_flexform';
ExtensionManagementUtility::addPiFlexFormValue(
	'sgjobs_joblist',
	'FILE:EXT:sg_jobs/Configuration/FlexForms/Joblist.xml'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['sgjobs_jobapplication'] = 'pi_flexform';
ExtensionManagementUtility::addPiFlexFormValue(
	'sgjobs_jobapplication',
	'FILE:EXT:sg_jobs/Configuration/FlexForms/JobApplication.xml'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['sgjobs_jobteaser'] = 'pi_flexform';
ExtensionManagementUtility::addPiFlexFormValue(
	'sgjobs_jobteaser',
	'FILE:EXT:sg_jobs/Configuration/FlexForms/JobTeaser.xml'
);
