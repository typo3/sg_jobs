<?php

$columns = [
	'ctrl' => [
		'title' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_company',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'searchFields' => 'job_id, zip, city, state, country, description, name, street, contact',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		],
		'sortby' => 'sorting',
		'iconfile' => 'EXT:sg_jobs/Resources/Public/Icons/tx_sgjobs_domain_model_company.svg',
		'security' => [
			'ignorePageTypeRestriction' => TRUE
		]
	],
	'interface' => [],
	'types' => [
		'1' => [
			'showitem' => '--palette--;;sysLanguageAndHidden,job_id,zip,city,street,name,state,country,description,contact,--palette--,--div--;LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tca.seo_tab,identifying_url,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime,hidden',
		],
	],
	'palettes' => [
		'sysLanguageAndHidden' => [
			'showitem' => 'sys_language_uid;;;;1-1-1, l10n_diffsource, hidden;;1, ',
			'canNotCollapse' => 1,
		]
	],
	'columns' => [
		'sys_language_uid' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
			'config' => ['type' => 'language'],
		],
		'l10n_parent' => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					['label' => '', 'value' => 0],
				],
				'foreign_table' => 'tx_sgjobs_domain_model_company',
				'foreign_table_where' => 'AND tx_sgjobs_domain_model_company.pid=###CURRENT_PID### AND tx_sgjobs_domain_model_company.sys_language_uid IN (-1,0)',
			],
		],
		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
			],
		],
		'pid' => [
			'exclude' => 0,
			'label' => 'PID',
			'config' => [
				'type' => 'none',
			]
		],
		'hidden' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
			'config' => [
				'type' => 'check',
			],
		],
		'starttime' => [
			'exclude' => TRUE,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
			'config' => [
				'type' => 'datetime',
				'format' => 'datetime',
				'size' => 13,
				'default' => 0
			],
		],
		'endtime' => [
			'exclude' => TRUE,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
			'config' => [
				'type' => 'datetime',
				'size' => 13,
				'format' => 'datetime',
				'default' => 0
			],
		],
		'zip' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_company.zip',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'city' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_company.city',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'job_id' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_company.job_id',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'name' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_company.name',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'street' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_company.street',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'state' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_company.state',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'country' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_company.country',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'description' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_company.description',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'contact' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.contact',
			'config' => [
				'type' => 'select',
				'items' => [['label' => 'None', 'value' => 0]],
				'renderType' => 'selectSingle',
				'foreign_table' => 'tx_sgjobs_domain_model_contact',
				'foreign_table_where' => 'tx_sgjobs_domain_model_contact.hidden = FALSE',
				'size' => 1,
				'maxitems' => 1,
				'multiple' => 0,
				'fieldControl' => [
					'editPopup' => [
						'disabled' => FALSE,
					],
					'addRecord' => [
						'disabled' => FALSE,
					]
				],
			],
		],
		'identifying_url' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_company.identifyingUrl',
			'config' => [
				'type' => 'link',
				'allowedTypes' => [
					'page', 'url', 'record'
				],
				'appearence' => [
					'allowedOptions' => [
						'params', 'rel'
					]
				]
			],
		],
	],
];

return $columns;
