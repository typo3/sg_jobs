<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addToAllTCAtypes(
	'tx_sgjobs_domain_model_job',
	''
);
$columns = [
	'ctrl' => [
		'title' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'searchFields' => 'title, job_id, path_segment, start_date, alternative_start_date, location, apply_external_link, description, task, qualification',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		],
		'sortby' => 'sorting',
		'iconfile' => 'EXT:sg_jobs/Resources/Public/Icons/tx_sgjobs_domain_model_job.svg',
		'security' => [
			'ignorePageTypeRestriction' => TRUE
		]
	],
	'interface' => [],
	'types' => [
		'1' => [
			'showitem' => '--palette--;;sysLanguageAndHidden,--palette--;;palette_title,--palette--;;palette_title_start_location,--palette--;;palette_apply_function,apply_external_link,department,experience_level,company,contact,related_jobs,--div--;LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tca.qualification_tab,description,task,qualification,attachment,--div--;LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tca.seo_tab,--palette--;;palette_location_specifications,employment_types,--palette--;;palette_seo_dates,--palette--;;palette_salary,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime,--div--;LLL:EXT:languagevisibility/Resources/Private/Language/locallang_db.xml:tabname,tx_languagevisibility_visibility',
		],
	],
	'palettes' => [
		'sysLanguageAndHidden' => [
			'showitem' => 'sys_language_uid;;;;1-1-1, l10n_diffsource, hidden;;1, ',
			'canNotCollapse' => 1,
		],
		'palette_title' => ['showitem' => 'title, path_segment, job_id', 'canNotCollapse' => 1],
		'palette_title_start_location' => ['showitem' => 'start_date, alternative_start_date, location', 'canNotCollapse' => 1],
		'palette_apply_function' => ['showitem' => 'hide_apply_by_email, hide_apply_by_postal, featured_offer', 'canNotCollapse' => 1],
		'palette_location_specifications' => ['showitem' => 'location_requirements, telecommute_possible, office_work_possible'],
		'palette_seo_dates' => ['showitem' => 'date_posted, valid_through'],
		'palette_salary' => [
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tca.salary_palette',
			'showitem' => 'salary_currency, --linebreak--, base_salary, max_salary, --linebreak--, salary_unit,'
		]
	],
	'columns' => [
		'sorting' => [
			'config' => [
				'type' => 'passthrough'
			]
		],
		'crdate' => [
			'exclude' => FALSE,
			'config' => [
				'type' => 'passthrough'
			]
		],
		'sys_language_uid' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
			'config' => ['type' => 'language'],
		],
		'l10n_parent' => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					['label' => '', 'value' => 0],
				],
				'foreign_table' => 'tx_sgjobs_domain_model_job',
				'foreign_table_where' => 'AND tx_sgjobs_domain_model_job.pid=###CURRENT_PID### AND tx_sgjobs_domain_model_job.sys_language_uid IN (-1,0)',
			],
		],
		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
			],
		],
		'tx_languagevisibility_visibility' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:languagevisibility/locallang_db.xlf:pages.tx_languagevisibility_visibility',
			'config' => [
				'type' => 'user',
				'renderType' => 'languageVisibility'
			]
		],
		'pid' => [
			'exclude' => FALSE,
			'label' => 'PID',
			'config' => [
				'type' => 'none',
			]
		],
		'hidden' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
			'config' => [
				'type' => 'check',
			],
		],
		'starttime' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
			'config' => [
				'type' => 'datetime',
				'default' => 0
			],
			'l10n_mode' => 'exclude',
			'l10n_display' => 'defaultAsReadonly'
		],
		'endtime' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
			'config' => [
				'type' => 'datetime',
				'default' => 0,
				'range' => [
					'upper' => mktime(0, 0, 0, 1, 1, 2038)
				]
			],
			'l10n_mode' => 'exclude',
			'l10n_display' => 'defaultAsReadonly'
		],
		'path_segment' => [
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.path_segment',
			'config' => [
				'type' => 'slug',
				'generatorOptions' => [
					'fields' => ['title'],
					'fieldSeparator' => '/',
					'prefixParentPageSlug' => FALSE,
					'replacements' => [
						'/' => '-',
					],
				],
				'fallbackCharacter' => '-',
				'eval' => 'uniqueInPid'
			]
		],
		'title' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.title',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'job_id' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.jobId',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'attachment' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.attachment',
			'config' => [
				'type' => 'file',
				'maxitems' => 1,
				'allowed' => ['pdf']
			]
		],
		'department' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.department',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'foreign_table' => 'tx_sgjobs_domain_model_department',
				'foreign_table_where' => ' AND tx_sgjobs_domain_model_department.sys_language_uid IN (0,-1) ORDER BY tx_sgjobs_domain_model_department.sorting ASC',
				'minitems' => 1,
				'maxitems' => 1,
				'fieldControl' => [
					'editPopup' => [
						'disabled' => FALSE,
					],
					'addRecord' => [
						'disabled' => FALSE,
					]
				]
			],
		],
		'experience_level' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.experience_level',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'foreign_table' => 'tx_sgjobs_domain_model_experience_level',
				'foreign_table_where' => ' AND tx_sgjobs_domain_model_experience_level.sys_language_uid IN (0,-1) ORDER BY tx_sgjobs_domain_model_experience_level.sorting ASC',
				'minitems' => 0,
				'maxitems' => 999,
				'fieldControl' => [
					'editPopup' => [
						'disabled' => FALSE,
					],
					'addRecord' => [
						'disabled' => FALSE,
					]
				]
			],
		],
		'featured_offer' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.featuredOffer',
			'config' => [
				'type' => 'check',
			],
		],
		'hide_apply_by_email' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.hideApplyByEmail',
			'config' => [
				'type' => 'check',
			],
		],
		'hide_apply_by_postal' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.hideApplyByPostal',
			'config' => [
				'type' => 'check',
			],
		],
		'start_date' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.start_date',
			'config' => [
				'type' => 'datetime',
				'size' => 13,
				'format' => 'date'
			],
		],
		'alternative_start_date' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.alternative_start_date',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'location' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.location',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'task' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.task',
			'config' => [
				'type' => 'text',
				'enableRichtext' => TRUE,
				'cols' => 40,
				'rows' => 10,
				'eval' => ''
			],
		],
		'qualification' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.qualification',
			'config' => [
				'type' => 'text',
				'enableRichtext' => TRUE,
				'cols' => 40,
				'rows' => 10,
				'eval' => ''
			],
		],
		'description' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.description',
			'config' => [
				'type' => 'text',
				'enableRichtext' => TRUE,
				'cols' => 40,
				'rows' => 10,
				'eval' => ''
			],
		],
		'company' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.company',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'foreign_table' => 'tx_sgjobs_domain_model_company',
				'foreign_table_where' => ' AND tx_sgjobs_domain_model_company.sys_language_uid IN (0,-1) ORDER BY tx_sgjobs_domain_model_company.sorting ASC',
				'minitems' => 1,
				'multiple' => 1,
				'fieldControl' => [
					'editPopup' => [
						'disabled' => FALSE,
					],
					'addRecord' => [
						'disabled' => FALSE,
					]
				],
			]
		],
		'contact' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.contact',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'foreign_table' => 'tx_sgjobs_domain_model_contact',
				'foreign_table_where' => ' AND tx_sgjobs_domain_model_contact.sys_language_uid IN (0,-1) ORDER BY tx_sgjobs_domain_model_contact.sorting ASC',
				'maxitems' => 1,
				'multiple' => 0,
				'fieldControl' => [
					'editPopup' => [
						'disabled' => FALSE,
					],
					'addRecord' => [
						'disabled' => FALSE,
					]
				],
			]
		],
		'location_requirements' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.location_requirements',
			'config' => [
				'type' => 'input',
			],
		],
		'telecommute_possible' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.telecommutePossible',
			'config' => [
				'type' => 'check',
			],
		],
		'office_work_possible' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.officeWorkPossible',
			'config' => [
				'type' => 'check',
			],
		],
		'employment_types' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.employment_types',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'items' => [
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.employment_types.full_time', 'value' => 'FULL_TIME'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.employment_types.part_time', 'value' => 'PART_TIME'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.employment_types.contractor', 'value' => 'CONTRACTOR'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.employment_types.temporary', 'value' => 'TEMPORARY'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.employment_types.intern', 'value' => 'INTERN'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.employment_types.volunteer', 'value' => 'VOLUNTEER'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.employment_types.per_diem', 'value' => 'PER_DIEM'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.employment_types.other', 'value' => 'OTHER']
				]
			]
		],
		'date_posted' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.date_posted',
			'config' => [
				'type' => 'datetime',
				'format' => 'date'
			],
		],
		'valid_through' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.valid_through',
			'config' => [
				'type' => 'datetime',
				'format' => 'date'
			],
		],
		'salary_currency' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.salary_currency',
			'config' => [
				'type' => 'input',
				'eval' => 'alpha, upper',
				'max' => 3,
				'default' => 'EUR',
			],
		],
		'salary_unit' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.salary_unit',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.salary_unit.hour', 'value' => 'HOUR'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.salary_unit.day', 'value' => 'DAY'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.salary_unit.week', 'value' => 'WEEK'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.salary_unit.month', 'value' => 'MONTH'],
					['label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.salary_unit.year', 'value' => 'YEAR'],
				]
			]
		],
		'base_salary' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.base_salary',
			'config' => [
				'type' => 'number',
				'max' => 15,
				'format' => 'decimal',
			],
		],
		'max_salary' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.max_salary',
			'config' => [
				'type' => 'number',
				'max' => 15,
				'format' => 'decimal',
			],
		],
		'apply_external_link' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.apply_external_link',
			'config' => [
				'type' => 'input',
				'size' => 512,
				'eval' => 'trim'
			],
		],
		'related_jobs' => [
			'exclude' => TRUE,
			'l10n_exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.related_jobs',
			'config' => [
				'type' => 'group',
				'allowed' => 'tx_sgjobs_domain_model_job',
				'size' => 5,
				'minitems' => 0,
				'maxitems' => 99
			],
		],
	],
];

return $columns;
