<?php

$columns = [
	'ctrl' => [
		'title' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application',
		'label' => 'last_name',
		'label_alt' => 'job_title',
		'label_alt_force' => 1,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'searchFields' => 'job_id, job, job_title, company, gender, first_name, last_name, street, city, zip, country, nationality, education, birth_date, phone, mobile, email, message, cover_letter, certificate, cv',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		],
		'sortby' => 'sorting',
		'iconfile' => 'EXT:sg_jobs/Resources/Public/Icons/tx_sgjobs_domain_model_job.svg',
		'security' => [
			'ignorePageTypeRestriction' => TRUE
		]
	],
	'interface' => [],
	'types' => [
		'1' => [
			'showitem' => '--palette--;;sysLanguageAndHidden,job_id,job,job_title,company,gender,first_name,last_name,street,city,zip,country,nationality,education,birth_date,phone,mobile,email,message,privacy_policy,cover_letter,cv,certificate,div,--palette--;;richtext[*]:rte_transform[mode=ts],--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime,--div--;LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tca.freetext_fields_tab,freetext_field1,freetext_field2,freetext_field3,freetext_field4,freetext_field5',
		],
	],
	'palettes' => [
		'sysLanguageAndHidden' => [
			'showitem' => 'sys_language_uid;;;;1-1-1, l10n_diffsource, hidden;;1, ',
			'canNotCollapse' => 1,
		]
	],
	'columns' => [
		'sys_language_uid' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
			'config' => ['type' => 'language'],
		],
		'l10n_parent' => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					['label' => '', 'value' => 0],
				],
				'foreign_table' => 'tx_sgjobs_domain_model_job_application',
				'foreign_table_where' => 'AND tx_sgjobs_domain_model_job_application.pid=###CURRENT_PID### AND tx_sgjobs_domain_model_job_application.sys_language_uid IN (-1,0)',
			],
		],
		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
			],
		],
		'pid' => [
			'exclude' => FALSE,
			'label' => 'PID',
			'config' => [
				'type' => 'none',
			]
		],
		'hidden' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
			'config' => [
				'type' => 'check',
			],
		],
		'starttime' => [
			'exclude' => TRUE,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
			'config' => [
				'type' => 'datetime',
				'format' => 'datetime',
				'size' => 13,
				'default' => 0
			],
		],
		'endtime' => [
			'exclude' => TRUE,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
			'config' => [
				'type' => 'datetime',
				'size' => 13,
				'format' => 'datetime',
				'default' => 0
			],
		],
		'job_id' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.job_id',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'job' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.job',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'tx_sgjobs_domain_model_job',
				'size' => 1,
				'maxitems' => 1,
				'multiple' => 0,
				'fieldControl' => [
					'editPopup' => [
						'disabled' => FALSE,
					],
					'addRecord' => [
						'disabled' => FALSE,
					]
				],
			]
		],
		'job_title' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.job_title',
			'config' => [
				'type' => 'input',
				'size' => 100,
				'eval' => 'trim'
			],
		],
		'company' => [
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job.company',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'foreign_table' => 'tx_sgjobs_domain_model_company',
				'foreign_table_where' => ' AND tx_sgjobs_domain_model_company.sys_language_uid IN (0,-1) ORDER BY tx_sgjobs_domain_model_company.sorting ASC',
				'minitems' => 0,
				'maxitems' => 20,
				'multiple' => 0,
				'fieldControl' => [
					'editPopup' => [
						'disabled' => FALSE,
					],
					'addRecord' => [
						'disabled' => FALSE,
					]
				],
			]
		],
		'gender' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.gender',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'first_name' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.first_name',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'last_name' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.last_name',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'street' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.street',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'city' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.city',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'zip' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.zip',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'country' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.country',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'nationality' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.nationality',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'education' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.education',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'birth_date' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.birth_date',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'phone' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.phone',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'mobile' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.mobile',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'email' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.email',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim, required'
			],
		],
		'message' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.message',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 10
			],
		],
		'cover_letter' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.cover_letter',
			'config' => [
				'type' => 'file',
				'maxitems' => 1,
				'minitems' => 1,
				'allowed' => ['pdf']
			]
		],
		'certificate' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.certificate',
			'config' => [
				'type' => 'file',
				'maxitems' => 1,
				'minitems' => 1,
				'allowed' => ['pdf']
			]
		],
		'cv' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.cv',
			'config' => [
				'type' => 'file',
				'maxitems' => 1,
				'minitems' => 1,
				'allowed' => ['pdf']
			]
		],
		'privacy_policy' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.privacy_policy',
			'config' => [
				'type' => 'check',
				'default' => 0
			]
		],
		'freetext_field1' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.freetext_field1',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'freetext_field2' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.freetext_field2',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'freetext_field3' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.freetext_field3',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'freetext_field4' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.freetext_field4',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'freetext_field5' => [
			'exclude' => FALSE,
			'label' => 'LLL:EXT:sg_jobs/Resources/Private/Language/locallang_db.xlf:tx_sgjobs_domain_model_job_application.freetext_field5',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		]
	],
];

return $columns;
