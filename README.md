# Ext: sg_jobs

<img src="https://www.sgalinski.de/typo3conf/ext/project_theme/Resources/Public/Images/logo.svg" />

License: [GNU GPL, Version 2](https://www.gnu.org/licenses/gpl-2.0.html)

Repository: https://gitlab.sgalinski.de/typo3/sg_jobs

Please report bugs here: https://gitlab.sgalinski.de/typo3/sg_jobs

## About

This extension provides job application functionality for a TYPO3 installation.

### Features:

* Creating jobs, including related details like companies and contacts
* Applying for the created jobs
* Applications include file uploads
* Every application is saved conveniently in a CSV format, along with the
  uploaded files
* Sends mail notifications when applications are submitted
* Provides a JobTitleMapper for the TYPO3 9 route enhancers

## Integration

Typoscript files need to be included manually.

```Typoscript
plugin.tx_sgjobs {
	settings {
		allowedFileExtensions - allowed file extensions for the uploads (default pdf)
		allowedMimeTypes - allowed mime types for the uploads (default application/pdf)
	}
}
```

## Upload file size

By default, the maximum file size for an upload is 5MB / 5000kB.
You can change this limit in the **constants.ts** configuration file:

```Typoscript
settings {
    .....
    # cat=plugin.tx_sgjobs/other; type=string; label=Allowed maximum file size for uploads in kB
    allowedMaxFileSize = 5000
}
```

## Backend Module

The Backend module is found in the **WEB** section under the name **Job Offers**.

You can create a new job offer by clicking on the **New Job Offer** button.

## Setting the contact email

When a user applies to a job offer an email will be sent. The adress of this email comes either from the
**contact person** assigned to the job, or if no contact person is set, the mail address is chosen from the selected
location.

Simply supply the uid of the contact record and every job without a contact person will use the desired contact.

## Setting the record page id for the Joblist plugin

You need to set the id of the page (or sys folder) where you store all your job offers, contacts and locations.
To do this you can select the page/folder as Record Storage Page in the plugin settings.

## Job form page

When inserting the joblist plugin on a page, make sure to select the page which
contains the application form from
Plugin -> Plugin Options -> Page containing the application form

## Automated Email Messages

The extension uses **sg_mail** to configure email templates.

## .htaccess

The folder which contains the applications folder should be blocked from outside
access from the .htaccess file. Example:

```ApacheConf
RedirectMatch 403 ^/fileadmin/JobApplication/.*$
```

## Implement job title mapping in TYPO3 9

To enable the job title mapping in TYPO3 9, you need to add this routeEnhancer configuration to your config.yaml.

```yaml
routeEnhancers:
    SgJobApplication:
        type: Extbase
        extension: SgJobs
        plugin: JobApplication
        routes:
            -   routePath: '/job/applyform/{job_title}'
                _controller: 'Joblist::applyForm'
                _arguments:
                    job_title: jobId
            -   routePath: '/job/apply'
                _controller: 'Joblist::apply'
        defaultController: 'Joblist::applyForm'
        aspects:
            job_title:
                type: JobTitleMapper
        SgJobList:
            type: Extbase
            extension: SgJobs
            plugin: Joblist
            routes:
                -   routePath: '/job/{job_title}'
                    _controller: 'Joblist::index'
                    _arguments:
                        job_title: jobId
            defaultController: 'Joblist::index'
            aspects:
                job_title:
                    type: JobTitleMapper
```

This configuration will use the JobTitleMapper to map the job title to the jobs is.
The routePath represents the speaking URL part where {job_title} is the placeholder for the job title.
The JobTitleMapper is assigned to the job_title placeholder via aspects and will take care of the encoding and decoding
of the job title and the according job id.

## Migration from version 1 to version 2

If you update from sg_jobs version 1, you need to execute the Upgrade Wizard that migrates the old area field.
To do so, please visit the Install Tool and click on Upgrade Wizards. Check the Job Upgrade Wizard and execute it.
This Wizard will migrate your area data into department records.

Also, please be aware of changes to the Fluid template regarding the area -> department migration and change the area
occurences of variables in the template to department records accordingly.

This version also introduces correct exclude field configurations for the tables, so check your backend user
and backend usergroup permissions and adjust them accordingly.

## Migration from version 3.2 to version 3.3

This is a purely optional step!

The new path_segment field was added to ensure that the proper URLs are used. In order to use it, add
this code to your routes configuration file.

Furthermore, go to the list module and open your jobs directory in the TYPO3 backend. Select in the fields list
the path_segment field after clicking on the jobs table to expand it to the table only view. You will get an edit
icon above the path_segment title now. Click on it and generate for all of you jobs a new URL.

```yaml
    SgJobApplication:
        type: Extbase
        extension: SgJobs
        plugin: JobApplication
        routes:
            -   routePath: '/{localizedSegment}/{jobTitle}'
                _controller: 'Joblist::applyForm'
                _arguments: { 'jobTitle': jobId }
            -   routePath: '/{localizedSegment}'
                _controller: 'Joblist::apply'
        defaultController: 'Joblist::applyForm'
        # The following issue prevents us from using this code. This patch is not good enough to use it yet as
        # slashes are not converted and this causes to regex mismatches in the routing environment.
        # https://review.typo3.org/c/Packages/TYPO3.CMS/+/61044
        # aspects:
        #     jobTitle:
        #         type: PersistedPatternMapper
        #         tableName: tx_sgjobs_domain_model_job
        #         routeFieldPattern: '-(?P<uid>\d+)$'
        #         routeFieldResult: '{title}-{uid}'
        aspects:
            jobTitle:
                type: PersistedAliasMapper
                tableName: tx_sgjobs_domain_model_job
                routeFieldName: path_segment
            localizedSegment:
                type: LocaleModifier
                default: 'apply'
                localeMap:
                    -   locale: 'de_DE.*'
                        value: 'bewerbung'
    SgJobList:
        type: Extbase
        extension: SgJobs
        plugin: Joblist
        routes:
            -   routePath: '/job/{jobTitle}'
                _controller: 'Joblist::index'
                _arguments: { 'jobTitle': jobId }
        defaultController: 'Joblist::index'
        aspects:
            jobTitle:
                type: PersistedAliasMapper
                tableName: tx_sgjobs_domain_model_job
                routeFieldName: path_segment
```
