/*
 *
 * Copyright notice
 *
 * (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

define([
	'jquery',
	'TYPO3/CMS/Backend/ModuleMenu',
	'TYPO3/CMS/Backend/Viewport'
], function ($, ModuleMenu, Viewport) {
	var SgJobs = {
		init: function() {
			$(document).ready(function() {
				$('.sg-jobs_pageswitch').on('click', function(event) {
					if(Viewport.NavigationContainer.PageTree !== undefined) {
						event.preventDefault();
						SgJobs.goTo('web_SgJobsBackend', event.target.dataset.page);
					}
				});
				$('#filter-reset-btn').on('click', function(event) {
					event.preventDefault();
					this.form.reset();
					$(this).closest('form').find('option:selected').each(function() {
						$(this).removeAttr('selected');
					});

					$('.reset-me').val('');

					this.form.submit();
				});
			});

			const paginators = document.querySelectorAll('.paginator-input');
			for (const paginator of paginators) {
				paginator.addEventListener('keypress', (event) => {
					if (event.keyCode === 13) {
						event.preventDefault();
						const page = event.currentTarget.value;
						const url = event.currentTarget.dataset.uri;
						self.location.href = url.replace('987654321', page);
					}
				})
			}
		},
		/**
		 * Switch the page and select it in the pagetree
		 *
		 * @param module
		 * @param id
		 */
		goTo: function(module, id) {
			var pageTreeNodes = Viewport.NavigationContainer.PageTree.instance.nodes;
			for (var nodeIndex in pageTreeNodes) {
				if (pageTreeNodes.hasOwnProperty(nodeIndex) && pageTreeNodes[nodeIndex].identifier === parseInt(id)) {
					Viewport.NavigationContainer.PageTree.selectNode(pageTreeNodes[nodeIndex]);
					break;
				}
			}
			ModuleMenu.App.showModule(module, 'id=' + id);
		}
	};
	TYPO3.SgJobs = SgJobs;

	SgJobs.init();

	return SgJobs;
});
