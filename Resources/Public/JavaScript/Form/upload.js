const { Dropzone } = window;

/**
 * This module handles the integration of Dropzone
 */
export default () => {
	if (!Dropzone) {
		return;
	}

	document.querySelectorAll('#apply').forEach((item) => {
		addFileUpload(item, 'coverLetter');
		addFileUpload(item, 'cv');
		addFileUpload(item, 'certificate');
	});

	function addFileUpload(item, uploadName) {
		const uploadBox = item.querySelector('.' + uploadName + '-upload');

		if (!uploadBox || !item) {
			return;
		}

		const {
			maxFileAmount,
			validFileExtensions,
			maxFileSize,
			innerText,
			cancelUpload,
			removeFile,
			fileTypeError,
			uploadAjax
		} = uploadBox.dataset;

		uploadBox.classList.add('dropzone');

		const dropzone = new Dropzone('#apply .' + uploadName + '-upload', {
			url: uploadAjax,
			addRemoveLinks: true,
			paramName: 'files',
			dictDefaultMessage: innerText,
			maxFilesize: maxFileSize,
			maxFiles: maxFileAmount,
			acceptedFiles: validFileExtensions,
			dictInvalidFileType: fileTypeError,
			dictCancelUpload: cancelUpload,
			dictRemoveFile: removeFile,
		});
		dropzone.on('success', (file) => handleUpload(file, item, uploadName));
		dropzone.on('removedfile', (file) => handleRemoval(file, item, uploadName));
	}

	function handleUpload(file, item, uploadName) {
		const { uuid } = file.upload;
		const response = JSON.parse(file.xhr.response);

		item.insertAdjacentHTML(
			'beforeend',
			`<input data-uuid='${uuid}' type='hidden' name='${uploadName}[${uuid}][uuid]' value='${uuid}'>
			<input data-uuid='${uuid}' type='hidden' name='${uploadName}[${uuid}][path]' value='${response.path}'>`,
		);
	}

	function handleRemoval(file, item, uploadName) {
		const { uuid } = file.upload;
		item.querySelectorAll(`[data-uuid='${uuid}']`).forEach((item) => {
			item.remove();
		});
	}
};
