import Upload from './Form/upload';

/**
 * This module handles the Isotope integration for sg_teaser
 */
export default class SgJobs {
	/**
	 * Kicks things off
	 */
	constructor(_root = {}) {
		Upload();
	}
}
