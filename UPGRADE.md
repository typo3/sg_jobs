## Version 6 Breaking Changes

- Dropped TYPO3 10 and 11 support
- Dropped PHP 7 support
- Removed DepartmentUpdateWizard
- Removed widget UriViewHelper
- Removed unused Common/setup.typoscript

## Version 5 Breaking Changes

- Dropped TYPO3 9 support
- Dropped php 7.3 support
- Dropped city, street, zip and hide_in_frontend fields from the contact table. There is no upgrade wizard. The fields
  were not used inside the main template/code since some time. The latter one was depcrated since a while. Adjust your
  local template to use the company fields instead.
- Job location field is deprecated, use different Companies instead; job.location is unused in default
  templates/settings
- Dropped support for sg_seo < 6.0
- Dropped support for sg_mail < 8.0

The handling of sorting inside plugins might have changed. Up until this release there was an extension setting, which
disabled
manual sorting by default and made order by title default, while the actual chosen plugin option was using manual
sorting.
In effect the default sorting was always by title. We removed the option to disable manual sorting, and switched the
values
inside the plugin settings. This way the default still is sorting by title, but with clearer intend. If you want manual
sorting,
you will need to edit your Plugins.

## Version 4 Breaking Changes

- Dropped TYPO3 8 support
