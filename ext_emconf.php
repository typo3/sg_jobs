<?php

$EM_CONF['sg_jobs'] = [
	'title' => 'Jobs',
	'description' => 'Manage and display your Job offers.',
	'category' => 'plugin',
	'version' => '6.2.5',
	'state' => 'stable',
	'author' => 'Stefan Galinski',
	'author_email' => 'stefan@sgalinski.de',
	'author_company' => 'sgalinski Internet Services (https://www.sgalinski.de)',
	'constraints' =>
		[
			'depends' =>
				[
					'typo3' => '12.4.0-12.4.99',
					'php' => '8.1.0-8.3.99',
					'sg_mail' => '>=7.0.0'
				],
			'conflicts' =>
				[
				],
			'suggests' =>
				[
				],
		],
];
