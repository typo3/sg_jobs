<?php

use SGalinski\SgAjax\Service\AjaxRegistration;
use SGalinski\SgJobs\Controller\Ajax\UploadController;
use SGalinski\SgJobs\Controller\JoblistController;
use SGalinski\SgJobs\Controller\JobTeaserController;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

call_user_func(
	static function () {
		// Configure frontend plugins
		ExtensionUtility::configurePlugin(
			'SgJobs',
			'Joblist',
			[
				// Available actions
				JoblistController::class => 'index'
			],
			[
				// Non-Cache actions
				JoblistController::class => 'index'
			]
		);
		ExtensionUtility::configurePlugin(
			'SgJobs',
			'JobApplication',
			[
				// Available actions
				JoblistController::class => 'applyForm, apply, empty'
			],
			[
				// Non-Cache actions
				JoblistController::class => 'applyForm, apply, empty'
			]
		);
		ExtensionUtility::configurePlugin(
			'SgJobs',
			'JobTeaser',
			[
				// Available actions
				JobTeaserController::class => 'index'
			],
			[
				// Non-Cache actions
				JobTeaserController::class => ''
			]
		);

		AjaxRegistration::configureAjaxFrontendPlugin(
			'sg_jobs',
			[
				UploadController::class => 'uploadCoverletter, uploadCv, uploadCertificate',
			]
		);

		ExtensionManagementUtility::addTypoScriptConstants(
			'@import "EXT:sg_jobs/Configuration/TypoScript/Backend/constants.typoscript"'
		);

		ExtensionManagementUtility::addTypoScriptSetup(
			'@import "EXT:sg_jobs/Configuration/TypoScript/Backend/setup.typoscript"'
		);

		// register mail templates
		$GLOBALS['sg_mail']['sg_jobs']['ApplicationMail'] = 'EXT:sg_jobs/Configuration/SgMail/ApplicationMail.php';
		$GLOBALS['sg_mail']['sg_jobs']['ApplicantMail'] = 'EXT:sg_jobs/Configuration/SgMail/ApplicantMail.php';

		//include Plugin sg_jobs
		ExtensionManagementUtility::addPageTSConfig(
			'@import "EXT:sg_jobs/Configuration/TsConfig/Page/NewContentElementWizard.tsconfig"'
		);

		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['languagevisibility']['recordElementSupportedTables']['tx_sgjobs_domain_model_job'] = [];
	}
);
