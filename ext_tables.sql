CREATE TABLE tx_sgjobs_domain_model_job
(
	path_segment           text,
	title                  text,
	job_id                 varchar(30)  DEFAULT ''    NOT NULL,
	attachment             int(11) DEFAULT '0' NOT NULL,
	task                   text,
	qualification          text,
	alternative_start_date text,
	start_date             int(11) unsigned DEFAULT '0' NOT NULL,
	location               text,
	location_requirements  text,
	company                text,
	telecommute_possible   tinyint(4) unsigned DEFAULT '0' NOT NULL,
	office_work_possible   tinyint(4) unsigned DEFAULT '1' NOT NULL,
	employment_types       text,
	date_posted            int(11) unsigned DEFAULT '0' NOT NULL,
	valid_through          int(11) unsigned DEFAULT '0' NOT NULL,
	salary_currency        varchar(3)   DEFAULT 'EUR' NOT NULL,
	base_salary            varchar(15)  DEFAULT ''    NOT NULL,
	max_salary             varchar(15)  DEFAULT ''    NOT NULL,
	salary_unit            varchar(5)   DEFAULT ''    NOT NULL,
	description            text,
	department             int(11) DEFAULT '0' NOT NULL,
	experience_level       varchar(255) DEFAULT '0'   NOT NULL,
	contact                int(11) unsigned DEFAULT '0' NOT NULL,
	featured_offer         tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hide_apply_by_email    tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hide_apply_by_postal   tinyint(4) unsigned DEFAULT '0' NOT NULL,
	apply_external_link    varchar(512) DEFAULT ''    NOT NULL,
	related_jobs           varchar(255) DEFAULT ''    NOT NULL
);

CREATE TABLE tx_sgjobs_domain_model_department
(
	title text
);

CREATE TABLE tx_sgjobs_domain_model_experience_level
(
	title text
);

CREATE TABLE tx_sgjobs_domain_model_company
(
	city            varchar(255) DEFAULT '' NOT NULL,
	zip             varchar(255) DEFAULT '' NOT NULL,
	name            varchar(255) DEFAULT '' NOT NULL,
	street          varchar(255) DEFAULT '' NOT NULL,
	state           varchar(255) DEFAULT '' NOT NULL,
	country         varchar(255) DEFAULT '' NOT NULL,
	description     text,
	contact         int(11) unsigned DEFAULT '0' NOT NULL,
	job_id          text,
	identifying_url varchar(255) DEFAULT '' NOT NULL
);

CREATE TABLE tx_sgjobs_domain_model_contact
(
	title      varchar(255) DEFAULT '' NOT NULL,
	first_name varchar(255) DEFAULT '' NOT NULL,
	last_name  varchar(255) DEFAULT '' NOT NULL,
	email      varchar(255) DEFAULT '' NOT NULL,
	phone      varchar(255) DEFAULT '' NOT NULL,
	city       varchar(255) DEFAULT '' NOT NULL,
	zip        varchar(255) DEFAULT '' NOT NULL,
	street     varchar(255) DEFAULT '' NOT NULL,
	state      varchar(255) DEFAULT '' NOT NULL,
	country    varchar(255) DEFAULT '' NOT NULL,
	image      int(11) unsigned DEFAULT '0'
);

CREATE TABLE tx_sgjobs_domain_model_job_application
(
	job_id          text,
	job             int(11) unsigned DEFAULT '0' NOT NULL,
	job_title       text,
	company         text,
	gender          varchar(30)  DEFAULT '' NOT NULL,
	first_name      text,
	last_name       text,
	street          text,
	city            text,
	zip             varchar(255) DEFAULT '' NOT NULL,
	country         text,
	nationality     text,
	education       text,
	birth_date      text,
	phone           text,
	mobile          text,
	email           text,
	message         text,
	cover_letter    int(11) unsigned DEFAULT '0' NOT NULL,
	certificate     int(11) unsigned DEFAULT '0' NOT NULL,
	cv              int(11) unsigned DEFAULT '0' NOT NULL,
	privacy_policy  tinyint(4) unsigned DEFAULT '0' NOT NULL,
	freetext_field1 text,
	freetext_field2 text,
	freetext_field3 text,
	freetext_field4 text,
	freetext_field5 text
);
